<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Brand_name extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
	}
    // public function index(){
    //    echo "sdsdsd";
    //    exit;
    // }	

	public function add_brand(){

		if(isset($_POST['submit']))
		{   
		    $brand_name =$this->input->post ('brand_name');

		    $a = $this->common_model->getData('car_name',array('car_name'=>$brand_name));
            
            if (count($a) > 0) {

            	$this->session->set_flashdata('failed' ,'This brnad name is alredy exist');	
			    redirect(base_url('brand_name/add_brand'));
            	
            }else
            {
	            $rest_image = '';
	            //print_r($_FILES['image']['name']);
	            //exit;      
	            if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']))
		           {
		             /* $_FILES['image']['name']= $_FILES['image']['name'];
		              $_FILES['image']['type']= $_FILES['image']['type'];
		              $_FILES['image']['tmp_name']= $_FILES['image']['tmp_name'];
		              $_FILES['image']['error']= $_FILES['image']['error'];
		              $_FILES['image']['size']= $_FILES['image']['size'];  */
		              //$config = array();
		              $config['width'] = "100";
					  $config['height'] = "100";
					  $config['max_size'] = "50";
		              $config['upload_path']   = 'uploads/brand_name/';
		              $config['allowed_types'] = 'jpg|jpeg|png|gif';

		               $config['file_name'] = militime.$_FILES['image']['name'];
		                    
		              $this->load->library('upload',$config);
		              $this->upload->initialize($config);
		              if($this->upload->do_upload('image'))
		              { //die('upload');
		                $image_data = $this->upload->data();
		                $rest_image =  $image_data['file_name'];
		              }else
		              {  
		              $err = $this->upload->display_errors();
		              $final_output = $this->session->set_flashdata('error_pic', $err);
		              redirect(base_url('brand_name/add_brand'));
		              }
		           }
                    //echo $rest_image;
                    //exit;
	            
	            	$insertid = $this->common_model->common_insert('car_name',array('car_name'=>$brand_name,'image'=>$rest_image,'status'=>1));
				 
					if($insertid == '' || $insertid == '0')
					{
					 	$this->session->set_flashdata('failed' ,'Something went wrong');	
					    redirect(base_url('brand_name/add_brand'));
					}
					else
					{   

		                $this->session->set_flashdata('success' ,'State added successfully');	
					    redirect(base_url('brand_name/brand_list'));
					}
            }		
		}
		//$data['subjects'] = $this->db->query("select * from course_subject")->result();		
    	$this->load->view('admin/user/brand_name/add_brand');
	}

    public function brand_list()
    	{
       $data['category_data'] = $this->common_model->getData('car_name',array(),'car_name_id','DESC');
	
	   $this->load->view('admin/user/brand_name/brand_list',$data);
    }

	public function change_status()
    {
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');     
        $update = $this->common_model->updateData("car_name",array('status'=>$status),array('car_name_id'=>$user_id));
          if($update)
          {
             echo '1000';exit; 
          }
    }

    public function edit($id  = false){

		if(isset($_POST['submit']))
		{   
		    $brand_name =$this->input->post ('brand_name');

		    $a = $this->common_model->getData('car_name',array('car_name'=>$brand_name));
            
            if (count($a) > 0) {

            	$this->session->set_flashdata('failed' ,'This brnad name is alredy exist');	
			    redirect(base_url('brand_name/brand_list'));
            	
            }else
            {
	            $rest_image = '';
	            //print_r($_FILES['image']['name']);
	            //exit;      
	            if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']))
		           {
		              $_FILES['image']['name']= $_FILES['image']['name'];
		              $_FILES['image']['type']= $_FILES['image']['type'];
		              $_FILES['image']['tmp_name']= $_FILES['image']['tmp_name'];
		              $_FILES['image']['error']= $_FILES['image']['error'];
		              $_FILES['image']['size']= $_FILES['image']['size'];  

		              $config = array();
		             // $config['width'] = 200;
                     // $config['height'] = 200;
		              $config['upload_path']   = 'uploads/brand_name/';
		              $config['allowed_types'] = 'jpg|jpeg|png|gif';

		              $config['file_name'] = militime.$_FILES['image']['name'];
		                    
		              $this->load->library('upload',$config);
		              $this->upload->initialize($config);
		              if($this->upload->do_upload('image'))
		              { 
		                $image_data = $this->upload->data();
		                $rest_image =  $image_data['file_name'];
		              }else
		              {  
		                $err= $this->upload->display_errors();
		                $final_output = $this->session->set_flashdata('error_pic', $err);
		              }
		           }else{

		           	  $data = $this->common_model->common_getRow('car_name',array('car_name_id'=>$id));
		           	  $rest_image = $data->image;
		           }
                    //echo $rest_image;
                    //exit;
	            
	            	$insertid = $this->common_model->updateData('car_name',array('car_name'=>$brand_name,'image'=>$rest_image),array('car_name_id'=>$id));
				 
					if($insertid == '' || $insertid == '0')
					{
					 	$this->session->set_flashdata('failed' ,'Something went wrong');	
					    redirect(base_url('brand_name/brand_list'));
					}
					else
					{   

		                $this->session->set_flashdata('success' ,'State added successfully');	
					    redirect(base_url('brand_name/brand_list'));
					}
            }		
		}
		$data['brand_data'] = $this->common_model->common_getRow('car_name',array('car_name_id'=>$id));
		//$data['subjects'] = $this->db->query("select * from course_subject")->result();		
    	$this->load->view('admin/user/brand_name/edit_brand',$data);
	}
}	