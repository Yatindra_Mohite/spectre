<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Incomplete_registration extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
		
	}

	public function index(){
		   $data['user_list'] = $this->db->query("select * from user where user.user_type=0")->result();
		   $this->load->view('admin/incomplete_user/user_list', $data);
	 }

   

	public function change_status(){
		//echo "hihih";
		//exit;
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');
              
        $update = $this->common_model->updateData("user",array('admin_status'=>$status),array('id'=>$user_id));
       
	    if($update)
	    {
	        echo '1000';exit; 
	    }
    }

    public function edit($id = false){
     $id = $this->common_model->id_decrypt($id);
		if(isset($_POST['submit']))
		{   
		     	$s = $this->input->post ('section_name');
			    
			     $s1 = $this->input->post ('positive_mark');
			    

			     $s2 = $this->input->post ('partial_mark');
			    

			     $s3 = $this->input->post ('negative_mark');

			$updateid = $this->common_model->updateData('marking',array('name'=>$s,'positive_mark'=>$s1,'partial_mark'=>$s2,'negative_mark'=>$s3),array('id'=>$id));
						if($updateid == '' || $updateid == '0') {
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			    	redirect(base_url('Marking_scheme'));
			}
			else
			{
        	    $this->session->set_flashdata('success' ,'Scheme updated successfully');	
			   redirect(base_url('Marking_scheme/scheme_list'));
			}	
		}

    $data['scheme_edit'] = $this->common_model->common_getRow('marking',array('id'=>$id));
    $this->load->view('admin/marking_scheme/edit_scheme', $data);
	 }

   public function add_question($id = false){
    
    $id = $this->common_model->id_decrypt($id);

    $data['que_active'] = $this->common_model->common_getRow('musp',array('id'=>$id));
    //print_r($data);
    //exit;

    $this->load->view('admin/musp/add_question',$data);
   
   }
}