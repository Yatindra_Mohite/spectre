<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Version_name extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
	}
    // public function index(){
    //    echo "sdsdsd";
    //    exit;
    // }	

	public function add_version(){

		if(isset($_POST['submit']))
		{   
		    $version_name =$this->input->post ('version_name');
		    $model_id =$this->input->post ('model_id');


		    $a = $this->common_model->getData('version_name',array('version_name'=>$version_name,'model_id'=>$model_id));
            
            if (count($a) > 0) {

            	$this->session->set_flashdata('failed' ,'This version name is alredy exist');	
			    redirect(base_url('version_name/add_version'));
            	
            }else
            {	            
	            	$insertid = $this->common_model->common_insert('version_name',array('version_name'=>$version_name,'model_id'=>$model_id,'status'=>1));
				 
					if($insertid == '' || $insertid == '0')
					{
					 	$this->session->set_flashdata('failed' ,'Something went wrong');	
					    redirect(base_url('version_name/add_version'));
					}
					else
					{   

		                $this->session->set_flashdata('success' ,'Version added successfully');	
					    redirect(base_url('version_name/version_list'));
					}
            }		
		}
		//$data['subjects'] = $this->db->query("select * from course_subject")->result();		
    	$this->load->view('admin/user/version_name/add_version');
	}

    public function version_list()
    	{
       $data['category_data'] = $this->common_model->getData('version_name',array(),'version_id','DESC');
	
	   $this->load->view('admin/user/version_name/version_list',$data);
    }

	public function change_status()
    {
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');     
        $update = $this->common_model->updateData("version_name",array('status'=>$status),array('version_id'=>$user_id));
          if($update)
          {
             echo '1000';exit; 
          }
    }

    public function edit($id  = false){

		if(isset($_POST['submit']))
		{   
		     $version_name =$this->input->post ('version_name');
		     $model_id =$this->input->post ('model_id');
           

		    $a = $this->common_model->getData('version_name',array('version_name'=>$version_name,'model_id'=>$model_id));
            
            if (count($a) > 0) {

            	$this->session->set_flashdata('failed' ,'This version name is alredy exist');	
			    redirect(base_url('version_name/version_list'));
            	
            }else
            {	            
	            	$insertid = $this->common_model->updateData('version_name',array('version_name'=>$version_name,'model_id'=>$model_id),array('version_id'=>$id));
				 
					if($insertid == '' || $insertid == '0')
					{
					 	$this->session->set_flashdata('failed' ,'Something went wrong');	
					    redirect(base_url('version_name/version_list'));
					}
					else
					{   

		                $this->session->set_flashdata('success' ,'Version Update successfully');	
					    redirect(base_url('version_name/version_list'));
					}
            }		
		}
		$data['version_data'] = $this->common_model->common_getRow('version_name',array('version_id'=>$id));
		//$data['subjects'] = $this->db->query("select * from course_subject")->result();		
    	$this->load->view('admin/user/version_name/edit_version',$data);
	}
}	