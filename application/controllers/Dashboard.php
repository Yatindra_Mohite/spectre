<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
		date_default_timezone_set('Asia/Kolkata');
	}
	
	public function index()
    {
		// $curdate=date("Y-m-d");
		// $date = strtotime("+7 day");
		// $curdate_7=date('Y-m-d', $date);
		
		// //get list leads/project data start
  //       $data['data']=$this->common_model->getDataField('projects.project_id, projects.project_name, projects.project_client_name, projects.project_client_email, projects.project_client_contact_no, projects.project_platforms, projects.project_manager_id, projects.project_current_status, projects.project_add_date, projects.project_estimate_budget, projects.project_budget_unit, projects.project_payment_amount, projects.project_payment_recieved_date, users.user_name, project_status.project_status_name, project_status.project_status_class_name, currency_unit.currency_unit_text','projects','','projects.project_id','DESC',array('users'=>'projects.project_manager_id=users.user_id','project_status'=>'project_status.project_status_id=projects.project_current_status', 'currency_unit'=>'projects.project_budget_unit=currency_unit.currency_unit_id'),'20','left');
		// //get list leads/project data end
		
		// //get list leads/project Today's Expected Payments data start
  //       $data['data_taday_payment']=$this->common_model->getDataField('projects.project_id, projects.project_name, projects.project_client_contact_no, projects.project_manager_id, projects.project_payment_amount, projects.project_payment_recieved_date, projects.project_current_status, users.user_name, currency_unit.currency_unit_text','projects',array('project_current_status!='=>'5', 'project_payment_recieved_date'=>$curdate),'projects.project_id','DESC',array('users'=>'projects.project_manager_id=users.user_id', 'currency_unit'=>'projects.project_budget_unit=currency_unit.currency_unit_id'),'','left');
		// //get list leads/project Today's Expected Payments data end
		
		// //get list leads/project this week Expected Payments data start
  //       $data['data_week_payment']=$this->common_model->getDataField('projects.project_id, projects.project_name, projects.project_client_contact_no, projects.project_manager_id, projects.project_payment_amount, projects.project_payment_recieved_date, projects.project_current_status, users.user_name, currency_unit.currency_unit_text','projects',array('project_current_status!='=>'5', 'project_payment_recieved_date > '=>$curdate, 'project_payment_recieved_date <= '=>$curdate_7),'projects.project_id','DESC',array('users'=>'projects.project_manager_id=users.user_id', 'currency_unit'=>'projects.project_budget_unit=currency_unit.currency_unit_id'),'','left');
		// //get list leads/project this week Expected Payments data end
		
		// //call Logs start
		// $data['data_call_log']=$this->common_model->getDataField('project_call_log.*, users.user_name, projects.project_name','project_call_log','','project_call_log.project_call_log_id','DESC',array('users'=>'project_call_log.user_id=users.user_id', 'projects'=>'project_call_log.project_id=projects.project_id'),'15','left');
		// //col logs end
		
		// //get list leads/project data start
  //       //$this->db->or_where('projects.project_add_by', $this->session->userdata('admin_id'));
		// //$this->db->or_where('projects.project_manager_id', $this->session->userdata('admin_id'));
		// $data['my_data']=$this->common_model->getDataField('projects.project_id, projects.project_name, projects.project_client_name, projects.project_client_email, projects.project_client_contact_no, projects.project_platforms, projects.project_manager_id, projects.project_current_status, projects.project_add_date, projects.project_estimate_budget, projects.project_budget_unit, projects.project_payment_amount, projects.project_payment_recieved_date, users.user_name, project_status.project_status_name, project_status.project_status_class_name, currency_unit.currency_unit_text','projects',array('projects.project_manager_id'=>$this->session->userdata('admin_id')),'projects.project_id','DESC',array('users'=>'projects.project_manager_id=users.user_id','project_status'=>'project_status.project_status_id=projects.project_current_status', 'currency_unit'=>'projects.project_budget_unit=currency_unit.currency_unit_id'),'20','left');
  //       //echo $this->db->last_query();exit;
		// //get list leads/project data end
		$this->load->view('admin/dashboard1');
	}
}
