 <?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {
public function __construct()
{
	parent::__construct();
	if(!$userid = $this->session->userdata('admin_id')){
		redirect(base_url('login'));
	}
	date_default_timezone_set('Asia/Kolkata');
	$militime =round(microtime(true) * 1000);
	$datetime =date('Y-m-d h:i:s');
	define('militime', $militime);
	define('datetime', $datetime);

}

public function content()
{
	$data['content_data'] = $this->common_model->getData('qalame_page_content',array(),'page_id','DESC');

	$this->load->view('admin/content/content',$data);	
} 

public function edit($id = false)
{ 
	$data['content'] = $this->common_model->common_getRow('qalame_page_content',array('page_id'=>$id));

	if($this->input->server('REQUEST_METHOD') === 'POST')
	{
	 	$content = array(
					'title' =>$this->input->post('title'),
					'page_detail' =>$this->input->post('description')
					);

	 	$update = $this->common_model->updateData('qalame_page_content',$content,array('page_id'=>$id));

	 	if($update)
		{
      		$this->session->set_flashdata('success', 'Content Updated Successfully.');
	  		redirect('setting/content');
		}
	} 

	$this->load->view('admin/content/edit',$data);	
}

public function version()
{
	 $data['androide_version'] = $this->common_model->common_getRow('app_version',array('version_id'=>1));

	 $data['ios_version'] = $this->common_model->common_getRow('app_version',array('version_id'=>2));

     $this->load->view('admin/content/version',$data);
}


 public function version_update()
  {
      if(isset($_POST['submit1']))
      {
              $min_android_version = $this->input->post('min_android_version');
              $current_android_version = $this->input->post('current_android_version');
              
             $version_update = $this->common_model->updateData('app_version',array('min_version'=> $min_android_version,'current_version'=>$current_android_version),array('version_id'=>'1'));

          if($version_update)
          {
               
                  $this->session->set_flashdata('success1',"Version updated Successfully");

                  redirect('setting/version');
          } 
          else
          {
            $this->session->set_flashdata('failed',"Version is Not Updated");

            redirect('setting/version');
          } 
            
      }
      else if(isset($_POST['submit2']))
      {
          $min_ios_version = $this->input->post('min_ios_version');
          $current_ios_version = $this->input->post('current_ios_version');
              
          $version_update = $this->common_model->updateData('app_version',array('min_version'=> $min_ios_version,'current_version'=>$current_ios_version),array('version_id'=>'2'));

          if($version_update)
          {
            $this->session->set_flashdata('success2',"Version updated Successfully");

            redirect('setting/version');
          } 
          else
          {
            $this->session->set_flashdata('failed',"Version is Not Updated");

            redirect('setting/version');
          }   
      
      }  
         redirect('setting/version');
  }


	
}
