<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Car_rent extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
	}

	public function car_rent_list()
	{
		$data['user_data'] = $this->common_model->getData('car_rent',array(),'rent_id','DESC');
		$this->load->view('admin/user/car_rent/car_rent_list',$data);
	}

	public function show($id = false)
	{
		$data['image_data'] = $this->common_model->getData('car_image',array('mix_id'=>$id,'type'=>2));
		
		$this->load->view('admin/user/car_rent/view_list',$data);
	}

	public function change_status()
    {
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');     
        $update = $this->common_model->updateData("car_rent",array('status'=>$status),array('rent_id'=>$user_id));
          if($update)
          {
             echo '1000';exit; 
          }
    } 

    public function delete_by_admin($id){
         //echo $id;
         $delete_data =$this->db->query("DELETE ut, ct, ft
		 FROM `car_rent` AS ut
		 LEFT OUTER JOIN `interest` AS ct on ut.rent_id = ct.interested_id
		 LEFT OUTER JOIN `notification` AS ft on ut.rent_id = ft.id 
		 WHERE ut.rent_id = '".$id."' AND ct.type = '2' AND ft.type = '2' ");
         
         if($delete_data) {
         	echo '1000'; exit;
         }
    }
}

?>	