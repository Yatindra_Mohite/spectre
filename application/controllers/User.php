<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
	}
	
	public function customer_list()
	{
		$data['user_data'] = $this->common_model->getData('user',array('type'=>'1'),'user_id','DESC');
		
		$this->load->view('admin/user/customer/customer_list',$data);
	}
	
	public function garage_list()
	{
		$data['user_data'] = $this->common_model->getData('user',array('type'=>'2'),'user_id','DESC');
		
		$this->load->view('admin/user/garage/garage_list',$data);
	}

	public function garage_details($id = false)
	{   
		$ids = $this->common_model->id_decrypt($id);
        $data['user_data'] = $this->common_model->common_getRow('user',array('user_id' =>$ids));

        $this->load->view('admin/user/garage/garage_details',$data);
	}

	public function change_status()
    {
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');     
        $update = $this->common_model->updateData("user",array('admin_status'=>$status),array('user_id'=>$user_id));
          if($update)
          {
             echo '1000';exit; 
          }
    }

    public function change_status2()
    {
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');     
        $update = $this->common_model->updateData("user",array('past_work'=>$status),array('user_id'=>$user_id));
          if($update)
          {
             echo '1000';exit; 
          }
    }
}
