<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Other extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
	}
	

	public function terms_and_conditions()
	{   
		$terms = $this->input->post('terms');

		if(isset($_POST['submit']))
		{   
			$updateid = $this->common_model->updateData('about_and_terms',array('terms'=>$terms),array('id'=>1));
			
			if($updateid == '' || $updateid == '0') {
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
				    redirect(base_url('other/terms_and_conditions'));
				}
				else
				{
	            $this->session->set_flashdata('success' ,'Terms and conditions update successfully');	
				   redirect(base_url('other/terms_and_conditions'));
				}
		}
		$data['user_data'] = $this->common_model->common_getRow('about_and_terms',array());
		
		$this->load->view('admin/user/other/terms',$data);
	}
    
    public function privacy_policy()
	{   
        $privicy = $this->input->post('privicy');

		if(isset($_POST['submit']))
		{   
			$updateid = $this->common_model->updateData('about_and_terms',array('privicy'=>$privicy),array('id'=>1));
			
			if($updateid == '' || $updateid == '0') {
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
				    redirect(base_url('other/privacy_policy'));
				}
				else
				{
	            $this->session->set_flashdata('success' ,'Privacy policy update successfully');	
				   redirect(base_url('other/privacy_policy'));
				}
		}
		$data['user_data'] = $this->common_model->common_getRow('about_and_terms',array());
		
		$this->load->view('admin/user/other/p&p',$data);
	}

	public function about_us_update()
	{   
		$about = $this->input->post('about_us');

		if(isset($_POST['submit']))
		{   
			$updateid = $this->common_model->updateData('about_and_terms',array('about_us'=>$about),array('id'=>1));
			
			if($updateid == '' || $updateid == '0') {
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
				    redirect(base_url('other/about_us_update'));
				}
				else
				{
	            $this->session->set_flashdata('success' ,'About us update successfully');	
				   redirect(base_url('other/about_us_update'));
				}
		}
		$data['user_data'] = $this->common_model->common_getRow('about_and_terms',array());

		$this->load->view('admin/user/other/about',$data);		
	}
	
}	