<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if($userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
		
	}
	
	public function index()
	{ 
		if(isset($_POST['submit']))
		{   
			     $username = $this->input->post ('username');
				 $password = md5($this->input->post('password'));
				 //exit;
				 $AdminData = $this->common_model->common_getRow('admin',array('user_email'=>$username,'user_password'=>$password));
				 //print_r($AdminData);
				 //exit;
				if (!empty($AdminData)) 
				{
						$this->session->set_userdata ( array (
								'admin_id'   => $AdminData->user_id,
								'user_id'   => $AdminData->user_id,
								'user_name'   => $AdminData->user_name,
								'user_email'   => $AdminData->user_email,
								));

							redirect(base_url().'dashboard');
				}
				else
				{
				  $this->session->set_flashdata('msg' ,'Email or Password Not Matched.');	
				  redirect(base_url('login'));	
				}	
		}
		$this->load->view('admin/index');
	}
	
}
