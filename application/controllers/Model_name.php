<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Model_name extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
	}
    // public function index(){
    //    echo "sdsdsd";
    //    exit;
    // }	

	public function add_model(){

		if(isset($_POST['submit']))
		{   
		    $model_name =$this->input->post ('model_name');
		    $car_id =$this->input->post ('car_id');


		    $a = $this->common_model->getData('model_name',array('model_name'=>$model_name,'car_name_id'=>$car_id));
            
            if (count($a) > 0) {

            	$this->session->set_flashdata('failed' ,'This Model name is alredy exist');	
			    redirect(base_url('model_name/add_model'));
            	
            }else
            {	            
	            	$insertid = $this->common_model->common_insert('model_name',array('model_name'=>$model_name,'car_name_id'=>$car_id,'status'=>1));
				 
					if($insertid == '' || $insertid == '0')
					{
					 	$this->session->set_flashdata('failed' ,'Something went wrong');	
					    redirect(base_url('model_name/add_model'));
					}
					else
					{   

		                $this->session->set_flashdata('success' ,'State added successfully');	
					    redirect(base_url('model_name/model_list'));
					}
            }		
		}
		//$data['subjects'] = $this->db->query("select * from course_subject")->result();		
    	$this->load->view('admin/user/model_name/add_model');
	}

    public function model_list()
    	{
       $data['category_data'] = $this->common_model->getData('model_name',array(),'model_id','DESC');
	
	   $this->load->view('admin/user/model_name/model_list',$data);
    }

	public function change_status()
    {
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');     
        $update = $this->common_model->updateData("model_name",array('status'=>$status),array('model_id'=>$user_id));
          if($update)
          {
             echo '1000';exit; 
          }
    }

    public function edit($id  = false){

		if(isset($_POST['submit']))
		{   
		     $car_id =$this->input->post ('car_id');
		     $model_name =$this->input->post ('model_name');
           

		    $a = $this->common_model->getData('model_name',array('model_name'=>$model_name,'car_name_id'=>$car_id));
            
            if (count($a) > 0) {

            	$this->session->set_flashdata('failed' ,'This Model name is alredy exist');	
			    redirect(base_url('model_name/model_list'));
            	
            }else
            {	            
	            	$insertid = $this->common_model->updateData('model_name',array('model_name'=>$model_name,'car_name_id'=>$car_id),array('model_id'=>$id));
				 
					if($insertid == '' || $insertid == '0')
					{
					 	$this->session->set_flashdata('failed' ,'Something went wrong');	
					    redirect(base_url('model_name/model_list'));
					}
					else
					{   

		                $this->session->set_flashdata('success' ,'Model Update successfully');	
					    redirect(base_url('model_name/model_list'));
					}
            }		
		}
		$data['model_data'] = $this->common_model->common_getRow('model_name',array('model_id'=>$id));
		//$data['subjects'] = $this->db->query("select * from course_subject")->result();		
    	$this->load->view('admin/user/model_name/edit_model',$data);
	}
}	