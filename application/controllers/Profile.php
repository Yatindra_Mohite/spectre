<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
		date_default_timezone_set('Asia/Kolkata');
	    
		$militime =round(microtime(true) * 1000);
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
	}
	
public function index()
{ 
   $uid = $this->session->userdata('admin_id');
   $data['admin_data'] = $this->common_model->common_getRow('admin',array('user_id'=>$uid));
   $this->load->view('admin/profile',$data);
}
  
// public function user_detail()
// {
//    $data['user_data'] = $this->common_model->getData('signup',array('del_status'=>0),'user_id','DESC');
//    //print_r($data['user_data']);exit;
//    $this->load->view('admin/user/user_detail',$data);

// }

public function check_password()
{
	$password = md5($this->input->post('password')); 
	$query = $this->db->query("SELECT `password` FROM `barber_admin` WHERE `admin_id` = 1")->row();
    if($query->password != $password)
    {
      echo "10000";
    }  
}

public function edit()
{ 
   $admin_id = $this->session->userdata('admin_id');
   $data['admin_data'] = $this->common_model->common_getRow('admin',array('user_id'=>$admin_id));

   if(isset($_POST['submit']))
   { 
        if(isset($_FILES['admin_img']['name']) && $_FILES['admin_img']['name'] != '')
	    {  
			$date = date("ymdhis");
			$config['upload_path'] = 'uploads/admin_image1/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			
			$subFileName = explode('.',$_FILES['admin_img']['name']);
			$ExtFileName = end($subFileName);
			$config['file_name'] = md5($date.$_FILES['admin_img']['name']).'.'.$ExtFileName;
            
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('admin_img'))
            { 
              $upload_data = $this->upload->data();
              $image = $upload_data['file_name'];

             
      
              ini_set("memory_limit", "-1");
                
              $config['image_library']  = 'gd2';
              $config['source_image']   = 'uploads/admin_image/'.$image;
              $config['create_thumb']   = TRUE;
              $config['maintain_ratio'] = TRUE;
              $config['width']          = "100";
              $config['height']         = "100";
			  $config['new_image'] = 'uploads/admin_image1/'.$image;
			  
			  $this->load->library('image_lib');
              $this->image_lib->initialize($config);
              $newimage =  $this->image_lib->resize();
              $this->image_lib->clear();
              $x12 = explode('.', $image);//for extension
              $thumb_img =  $x12[0].'.'.$x12[1];
            }
            else
			{  
				$this->data['err']= $this->upload->display_errors();
				$this->session->set_flashdata('error_pic', $this->data['err']);		
				redirect('profile');
			}
		
	   }
	 else
	 { 
	   $thumb_img = $data['admin_data']->user_profile_pic;
	 }
   	   $admin_data = array(
					'user_name'=>$this->input->post('name'),
					'user_profile_pic'=>$thumb_img,
					'user_email'=>$this->input->post('email')
					);
   	 }
   	 
   	 if(isset($_POST['submit1']))
   	 {
   	 	$old_password = $this->input->post('O_password');
        $old_password1 = md5($old_password);
        
        $data['admin_data'] = $this->common_model->common_getRow('admin',array('user_id'=>$admin_id));
        //echo $data['admin_data']->user_password;
        //exit;
        if($data['admin_data']->user_password!=$old_password1)
		{ 
			$this->session->set_flashdata('fail', 'Invalid old password');
			redirect('profile');
		}
		
   	 	   $admin_data = array(
					  'user_password'=>md5($this->input->post('c_password')),
					);

   	 }	
	     $update = $this->common_model->updateData('admin',$admin_data,array('user_id'=>$admin_id));

   	  $this->session->set_flashdata('success', 'updated Successfully.');
      redirect('profile');
  	 
}

}
