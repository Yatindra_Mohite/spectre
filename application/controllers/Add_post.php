<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Add_post extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
	}

	public function add_post_list()
	{
		$data['post_data'] = $this->common_model->getData('add_post',array(),'add_id','DESC');
		
		$this->load->view('admin/user/add_post/add_post_list',$data);
	}
	
    public function show($id = false)
	{
	    $data['image_data'] = $this->common_model->getData('car_image',array('mix_id'=>$id));
		
		$this->load->view('admin/user/add_post/view_list',$data);
	}

	public function change_status()
    {
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');     
        $update = $this->common_model->updateData("add_post",array('status'=>$status),array('add_id'=>$user_id));
          if($update)
          {
             echo '1000';exit; 
          }
    }

    public function delete_by_admin($id){
         //echo $id;
         $delete_data =$this->db->query("DELETE ut, ct, ft
		 FROM `add_post` AS ut
		 LEFT OUTER JOIN `interest` AS ct on ut.add_id = ct.interested_id
		 LEFT OUTER JOIN `notification` AS ft on ut.add_id = ft.id 
		 WHERE ut.add_id = '".$id."' AND ct.type = '1' AND ft.type = '1' ");
         
         if($delete_data) {
         	echo '1000'; exit;
         }
    }     
}
