<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Garage_work extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
	}

	public function garage_list()
	{
		$data['user_data'] = $this->common_model->getData('garage_work',array(),'work_id','DESC');
		$this->load->view('admin/user/garage_work/garage_work_list',$data);
	}

	public function show($id = false)
	{
		$data['image_data'] = $this->common_model->getData('car_image',array('mix_id'=>$id,'type'=>3));
		
		$this->load->view('admin/user/garage_work/view_list',$data);
	}

	public function change_status()
    {
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');     
        $update = $this->common_model->updateData("garage_work",array('status'=>$status),array('work_id'=>$user_id));
          if($update)
          {
             echo '1000';exit; 
          }
    }
}
?>	