<!DOCTYPE html>
<html>

<!-- Head -->
<head>

<title>Engineerbabu</title>

<!-- Meta-Tags -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- //Meta-Tags -->

<!-- Style --> <link rel="stylesheet" href="<?php echo base_url()?>template/login/style.css" type="text/css" media="all">

<!-- Fonts -->
<link href="//fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
<!-- //Fonts -->

</head>
<!-- //Head -->

<!-- Body -->
<body>

	<div class="logo">
            <a href="#">
                <img style=" padding-bottom: 10px; margin-top: 55px;" src="<?php echo base_url()?>uploads/Lead-Management-Logo4.png" alt="" /> </a>
        </div>
				
	<div class="w3layoutscontaineragileits">
		<form id="form11" class="login-form" action="<?php echo base_url().'login';?>" method="post" data-parsley-validate=''>
			<input type="text" Name="username" placeholder="EMAIL"  required="">
			<input type="password" Name="password" placeholder="PASSWORD" required="">
			
			<div class="aitssendbuttonw3ls">
				<input name="submit" type="submit" value="LOGIN">
			</div>
			<br>
			<?php if($this->session->flashdata('msg')){?>
                	<div class="alert alert-danger">
                        <span> <?php echo $this->session->flashdata('msg');?></span>
                    </div>
                <?php }?>
		</form>
	</div>
	<div class="w3footeragile">
		<p> &copy; 2017 Engineerbabu. All Rights Reserved | Design by <a href="https://www.engineerbabu.com/" target="_blank">Engineerbabu</a></p>
	</div>

</html>