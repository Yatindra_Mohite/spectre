<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Edit | Profile</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
       <?php $this->load->view('admin/head'); ?>
        <style type="text/css">
            .profile-userpic img{ max-width: 120px; }
        </style>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
        <?php $this->load->view('admin/new_header1'); ?>
        <div class="clearfix"> </div>
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?php $this->load->view('admin/new_sidebar1'); ?>
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Admin Profile </h1>
                        </div>
                    </div>
                    <ul class="page-breadcrumb breadcrumb">
                        <?php if(!empty($this->session->flashdata('success'))){echo "<span style='color:green'>".$this->session->flashdata('success')."</span>"; }?>
                    </ul>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PROFILE SIDEBAR -->
                            <div class="profile-sidebar">
                                <!-- PORTLET MAIN -->
                                <div class="portlet light profile-sidebar-portlet bordered">
                                    <!-- SIDEBAR USERPIC -->
                                    <div class="profile-userpic">
                                   <?php  
                                       if(!empty($admin_data->user_profile_pic))
                                       {
                                         $image = $admin_data->user_profile_pic;

                                         
                                      }else{ $image = 'default-medium.png';}
                                     ?>    
                                    <img  src="<?php echo base_url('uploads/admin_image1/'.$image);?>" class="img-responsive img-circle" alt="" width="30px" height="30px"> </div>
                                    <div class="profile-usertitle">
                                        <div class="profile-usertitle-name"> <?php if(isset($admin_data->user_name)){echo $admin_data->user_name;}?> 
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet light bordered">
                                    <div class="caption caption-md">
                                        <i class="icon-globe theme-font hide"></i>
                                        <span class="caption-subject font-blue-madison bold uppercase">Contact Details</span>
                                    </div>
                                    <div>
                                        <div class="margin-top-20 profile-desc-link">
                                            <i class="fa fa-envelope-o"></i>
                                            <?php if(isset($admin_data->user_email)){ echo $admin_data->user_email; } ?>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="profile-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption caption-md">
                                                    <i class="icon-bar-chart theme-font hide"></i>
                                                    <span class="caption-subject font-blue-madison bold uppercase">Update Information</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="table-scrollable table-scrollable-borderless">
                                                    <form id="login-form" action="<?php echo base_url('profile/edit');?>" method="post" id="profile_form" data-parsley-validate="" enctype="multipart/form-data" >
                                                    <input type="hidden" name="admin_id1" valu="<?php if($admin_data){echo $admin_data->user_id;}?>">
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <input class="form-control" name="name" type="text" placeholder="Name" value="<?php if($admin_data){echo $admin_data->user_name;}?>" data-parsley-pattern="^[A-Za-z ]*$" required>
                                                            </div>
                                                            <div class="form-group">
                                                             
                                                                <input class="form-control" data-parsley-type="email" name="email" type="text" placeholder="Email" data-parsley-type="email" value="<?php if($admin_data){echo $admin_data->user_email;}?>" required >
                                                            </div>
                                                            <div class="form-group">
                                                                <input class="form-control" name="admin_img" type="file" >
                                                               <?php if($this->session->flashdata('error_pic')){ echo "<div style='color:red;list-style-type: none;font-size: 0.9em;line-height: 0.9em;'>",$this->session->flashdata('error_pic'),"</div>"; }?>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                        <input  type="submit" name="submit" class="btn btn-primary" value="Submit">

                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption caption-md">
                                                    <i class="icon-bar-chart theme-font hide"></i>
                                                    <span class="caption-subject font-blue-madison bold uppercase">Update password</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="table-scrollable table-scrollable-borderless">
                                                    <form id="login_form" action="<?php echo base_url('profile/edit');?>" method="post" data-parsley-validate="">
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <input class="form-control" id="old_password" name="O_password" type="password" placeholder="Old Password" required>
                                                                <span id="err" style="color:red;margin-left:0px;list-style-type: none;font-size: 0.9em;line-height: 0.9em;"></span>
                                                                <?php if(!empty($this->session->flashdata('fail'))){echo "<span style='color:red'>".$this->session->flashdata('fail')."</span>"; }?>
                                                            </div>
                                                            <div class="form-group">
                                                                <input class="form-control" id="password" name="password" type="password" minlength="6" maxlength="10" placeholder="New Password" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <input class="form-control" data-parsley-equalto="#password" data-parsley-required-message="New Password & Confiem New Password should be matched" name="c_password" type="password" placeholder="Confirm Password"  required>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                        <input type="submit" name="submit1" id="submit1" class="btn btn-primary" value="Submit">

                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                             
                            </div>
                            <!-- END PROFILE CONTENT -->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
      
        <?php $this->load->view('admin/footer');?>
      </body>
       
<script type="text/javascript">
  $('#profile_form').parsley();
  $('#login_form').parsley();
</script>
  
<script>
$(document).ready(function(){
    $("#submit1").click(function(){   
        var password  = $("#old_password").val();

        if(password !== '')
        {

         
        var str = 0;
             $.ajax({
                    type:'POST',
                    url: '<?php echo base_url();?>profile/check_password',
                    data: "password="+password,
                    async: false,
                    success: function(data){ 
                        if(data=='10000')
                        {
                          str = 1;  
                          $("#err").html('Old Password is invalid').css('color','red');
                          
                        }
                        else
                        { 
                          $("#err").html('');
                        }
                    },
        
                });
            if(str == 1)
            {
                return false;
                
            } 

        }
        else
        {
            $("#err").html('Old Password is Required').css('color','red');
            return false;
        }  

        });

});

</script>     
</html>