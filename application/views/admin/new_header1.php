 <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="<?php echo base_url('dashboard');?>">
                        <img src="<?php echo base_url('uploads/Spectre-Logo120x40.png');?>" style="margin-top: 3px;" alt="logo" /></a>    
                        </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                    <span></span>
                    </a>
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                 
                                <?php
                                $userid = $this->session->userdata('admin_id'); 
                                $admin_data =  $this->common_model->common_getRow('admin',array('user_id'=>$userid));
                                        if(!empty($admin_data->user_profile_pic))
                                        {
                                            $image = $admin_data->user_profile_pic;
                                        } 
                                        else
                                        {
                                           $image = 'default-medium.png';
                                        }   
                                ?>
                                    <img alt="" class="img-circle" src="<?php echo base_url('uploads/admin_image1/'.$image)?>" />
                                    <span class="username username-hide-on-mobile"><?php if(!empty($this->session->userdata('user_name'))){ echo $this->session->userdata('user_name');}else { echo 'Admin';} ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="<?php echo base_url('profile');?>">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                   <!--  <li>
                                        <a href="app_inbox.html">
                                            <i class="icon-envelope-open"></i> My Inbox
                                            <span class="badge badge-danger"> 3 </span>
                                        </a>
                                    </li> -->
                                  <!--   <li>
                                        <a href="app_todo.html">
                                            <i class="icon-rocket"></i> My Tasks
                                            <span class="badge badge-success"> 7 </span>
                                        </a>
                                    </li> -->
                                   <!--  <li class="divider"> </li> -->
                                   <!--  <li>
                                        <a href="page_user_lock_1.html">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li> -->
                                    <li>
                                        <a href="<?php echo base_url('logout');?>">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                          
                            <!-- <li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li> -->
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>