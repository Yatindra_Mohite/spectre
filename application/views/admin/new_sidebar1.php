
<style type="text/css">
    .notificationNo{
    background: #8700c9;
    float: right;
    width: 20px;
    height: 20px;
    text-align: center;
    border-radius: 50%;
    font-size: 10px;
    line-height: 20px;
    color: #fff;
    }
</style>

 <div class="page-sidebar-wrapper">
                    
                    <div class="page-sidebar navbar-collapse collapse">
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 35px">
                           
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                           <!-- <li class="sidebar-search-wrapper">
                                <form class="sidebar-search  " action="page_general_search_3.html" method="POST">
                                    <a href="javascript:;" class="remove">
                                        <i class="icon-close"></i>
                                    </a>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <a href="javascript:;" class="btn submit">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </span>
                                    </div>
                                </form>
                            </li> -->
                            <li class="nav-item start <?php if($this->uri->segment(1)=='dashboard'){ echo 'active';} ?>">
                                <a href="<?php echo base_url('dashboard');?>" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="selected"></span>
                                    <!--<span class="arrow open"></span> -->
                                </a>
                            </li>    
                               <!--<ul class="sub-menu">
                                    <li class="nav-item start active open">
                                        <a href="index.html" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Dashboard 1</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>
                                </ul> -->
                            <li class="nav-item  <?php if($this->uri->segment(1)=='user'){ echo 'active';}?>">
                                 <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-user"></i>
                                    <span class="title">Users</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='user' && $this->uri->segment(2)=='garage_list'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('user/garage_list');?>" class="nav-link">
                                            <span class="title">Garage</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='user' && $this->uri->segment(2)=='customer_list'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('user/customer_list');?>" class="nav-link">
                                            <span class="title">Customer</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item  <?php if($this->uri->segment(1)=='add_post'){ echo 'active';}?>">
                                 <a href="<?php echo base_url('add_post/add_post_list');?>" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-paperclip"></i>
                                    <span class="title">Add Post</span>
                                    <?php $a = $this->common_model->sqlcount('add_post',array('notify'=>0));
                                          if ($a >= 1) {?>
                                             <span class="notificationNo"><?php echo $this->common_model->sqlcount('add_post',array('notify'=>0));?></span>
                                        <?php  }?>
                                </a>
                            </li>

                            <li class="nav-item  <?php if($this->uri->segment(1)=='car_rent'){ echo 'active';}?>">
                                 <a href="<?php echo base_url('car_rent/car_rent_list');?>" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-bed"></i>
                                    <span class="title">Car Rent</span>
                                    <?php $b = $this->common_model->sqlcount('car_rent',array('notify'=>0));
                                          if ($b >= 1) {?>
                                             <span class="notificationNo"><?php echo $this->common_model->sqlcount('car_rent',array('notify'=>0));?></span>
                                        <?php  }?>
                                </a>
                            </li>
                            <li class="nav-item  <?php if($this->uri->segment(1)=='garage_work'){ echo 'active';}?>">
                                 <a href="<?php echo base_url('garage_work/garage_list');?>" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-bed"></i>
                                    <span class="title">Garage Work</span>
                                    <?php $c = $this->common_model->sqlcount('garage_work',array('notify'=>0));
                                          if ($c >= 1) {?>
                                             <span class="notificationNo"><?php echo $this->common_model->sqlcount('garage_work',array('notify'=>0));?></span>
                                        <?php  }?>
                                </a>
                            </li>
                            <li class="nav-item  <?php if($this->uri->segment(1)=='brand_name'){ echo 'active';}?>">
                                 <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-apple"></i>
                                    <span class="title">Brand</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='brand_name' && $this->uri->segment(2)=='add_brand'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('brand_name/add_brand');?>" class="nav-link">
                                            <span class="title">Add Brand </span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='brand_name' && $this->uri->segment(2)=='brand_list'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('brand_name/brand_list');?>" class="nav-link">
                                            <span class="title">Brand List</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  <?php if($this->uri->segment(1)=='model_name'){ echo 'active';}?>">
                                 <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-triangle-right"></i>
                                    <span class="title">Model</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='model_name' && $this->uri->segment(2)=='add_model'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('model_name/add_model');?>" class="nav-link">
                                            <span class="title">Add Model </span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='model_name' && $this->uri->segment(2)=='model_list'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('model_name/model_list');?>" class="nav-link">
                                            <span class="title">Model List</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  <?php if($this->uri->segment(1)=='version_name'){ echo 'active';}?>">
                                 <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-triangle-right"></i>
                                    <span class="title">Version</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='version_name' && $this->uri->segment(2)=='add_version'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('version_name/add_version');?>" class="nav-link">
                                            <span class="title">Add Version </span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='version_name' && $this->uri->segment(2)=='version_list'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('version_name/version_list');?>" class="nav-link">
                                            <span class="title">Version List</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  <?php if($this->uri->segment(1)=='other' && $this->uri->segment(2)=='about_us'){ echo 'active';}?>">
                                 <a href="<?php echo base_url('other/about_us_update');?>" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-book"></i>
                                    <span class="title">About Us</span>
                                </a>
                            </li>
                            <li class="nav-item  <?php if($this->uri->segment(1)=='other' && $this->uri->segment(2)=='terms_and_conditions'){ echo 'active';}?>">
                                 <a href="<?php echo base_url('other/terms_and_conditions');?>" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-align-justify"></i>
                                    <span class="title">Terms and Conditions</span>
                                </a>
                            </li>
                            <li class="nav-item  <?php if($this->uri->segment(1)=='other' && $this->uri->segment(2)=='privacy_policy'){ echo 'active';}?>">
                                 <a href="<?php echo base_url('other/privacy_policy');?>" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-info-sign"></i>
                                    <span class="title">privacy policy</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END SIDEBAR -->
                </div>