<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Garage Details</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
           <?php $this->load->view("admin/new_header1"); ?>
            <!-- END HEADER INNER -->
        </div>
     
        <div class="clearfix"></div>
      
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
             <?php $this->load->view("admin/new_sidebar1"); ?>
         
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                 <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Garage Details
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Garage Details</span>
                        </li>
                    </ul>
                    <!-- BEGIN PAGE HEAD-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <?php if($this->session->flashdata('failed')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Garage Details</div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center> S.No </center></th>
                                                <th><center> Expertise </center></th>
                                                <th><center> Car Repaire </center></th>
                                                <th><center> Car Modify </center></th>
                                                <th><center> Status </center></th>


                                                <!-- <th><center>Question Time </center></th>
                                                <th><center>Total Time</center></th>
                                                <th><center>Pos. Sectional Mark </center></th>
                                                <th><center>Neg. Sectional Mark </center></th>  -->
                                                <!-- <th><center>Status</center></th> -->
                                                <!-- <th><center>Action</center></th> -->
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center> S.No </center></th>
                                                <th><center> Expertise </center></th>
                                                <th><center> Car Repaire </center></th>
                                                <th><center> Car Modify </center></th>
                                                <th><center> Status </center></th>

                                                <!-- <th><center>Question Time </center></th>
                                                <th><center>Total Time</center></th>
                                                <th><center>Pos. Sectional Mark </center></th>
                                                <th><center>Neg. Sectional Mark </center></th>  -->
                                                <!-- <th><center> Status </center></th> -->
                                                <!-- <th><center> Action </center></th> -->
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($user_data))
                                        { ?>
                                            

                                              
                                            <tr>
                                                    <td><center> 1 </center></td> 
                                                    <td><center><?php echo $user_data->expertise;?></center></td>
                                                    <td><center><?php echo $user_data->car_repaire;?></center></td>
                                                    <td><center><?php if ($user_data->car_modify == 1){echo "Yes";}else{echo "No";}?></center></td>
                                                    <td><center><select style="" class="form-control" onchange="changestatus('<?php echo $user_data->user_id;?>',this.value)">
                                                    <option value="0"<?php if($user_data->past_work==0){ echo 'selected';} ?>> Approve  </option>
                                                    <option value="1"<?php if($user_data->past_work==1){ echo 'selected';} ?>> Disaprove </option>
                                                    
                                                    </select></center></td>
                                            </tr>
                                         <?php }else{?>       
                                        <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class="" ></td>
                                                <td class="" ></td>

                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
      <?php $this->load->view("admin/footer"); ?>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>

<script type="text/javascript">
        function changestatus(id,status)
        { 
            var str = "user_id="+id+"&admin_status="+status;
            //alert(str);
            var r = confirm('Are you really want to change status?');
            if(r==true)
            {
                $.ajax({
                  type:"POST",
                   url:"<?php echo base_url('user/change_status2')?>/",
                   data:str,
                   success:function(data)
                   {   //alert(data);
                      if(data==1000)
                       {
                            location.reload();
                       }
                   }
                });
            }else{'hihih'}location.reload();
        }
    </script>




     


