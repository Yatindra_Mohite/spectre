<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Brand
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Add Brand</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Brand</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           if($this->session->flashdata('error_pic'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('error_pic'),"</div>"; 
           }
           ?>
            <form action="" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Add Brand<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="text" placeholder="Brand Name" name="brand_name" class="form-control" data-parsley-required-message="brand name is required"  required/>
                        </div>
                    </div>
                </div>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Select Logo<span class="required">*</span></label>
                        <div class="col-md-6">
                             <input type="file" name="image" required class="form-control" required=""/>
                        </div><br><br>
                        <div><span style="color:red;margin-left:350px; list-style-type: none;font-size: 0.9em;line-height: 0.9em;">Note: Image upload size is 100x100</span></div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" name="submit" value="Submit" >
                            <!-- <a href="<?php echo base_url()?>add_state"><button type="button" class="btn default">Cancel</button></a> -->
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>

<!-- <script type="text/javascript">
 $( function() {
    $( "#date1" ).datepicker({
       changeMonth: true,
       changeYear: true,
       dateFormat:'yy-mm-dd',
       minDate: 0 
    });
  } );
  </script> -->        
    </body>

</html>