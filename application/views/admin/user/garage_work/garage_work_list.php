<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Garage Work</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
           <?php $this->load->view("admin/new_header1"); ?>
            <!-- END HEADER INNER -->
        </div>
     
        <div class="clearfix"></div>
      
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
             <?php $this->db->query("UPDATE garage_work SET notify = '1'");?>

             <?php $this->load->view("admin/new_sidebar1"); ?>
         
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                 <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Garage Work
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Garage Work</span>
                        </li>
                    </ul>
                    <!-- BEGIN PAGE HEAD-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <?php if($this->session->flashdata('failed')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Garage Work</div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center> S.No </center></th>
                                                <th><center> Full Name </center></th>
                                                <th><center> Car Name </center></th>
                                                <th><center> Model </center></th>
                                                <th><center> Colour </center></th>
                                                <th><center> Car Type </center></th>
                                                <th><center> Condition </center></th>
                                                <th><center> Version </center></th>
                                                <th><center> Year </center></th>
                                                <th><center> Mileage</center></th>
                                                <th><center> Price </center></th>
                                                <th><center> Problem </center></th>
                                                <th><center> View </center></th>
                                                <!-- <th><center>Neg. Sectional Mark </center></th>  -->
                                                <th><center>Status</center></th>
                                                <!-- <th><center>Action</center></th> -->
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center> S.No </center></th>
                                                <th><center> Full Name </center></th>
                                                <th><center> Car Name </center></th>
                                                <th><center> Model </center></th>
                                                <th><center> Colour </center></th>
                                                <th><center> Car Type </center></th>
                                                <th><center> Condition </center></th>
                                                <th><center> Version </center></th>
                                                <th><center> Year </center></th>
                                                <th><center> Mileage</center></th>
                                                <th><center> Price </center></th>
                                                <th><center> Problem </center></th>
                                                <th><center> View </center></th>
                                                <!-- <th><center>Neg. Sectional Mark </center></th>  -->
                                                <th><center>Status</center></th>
                                                <!-- <th><center>Action</center></th> -->
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($user_data))
                                        {   $i = 0;
                                            foreach($user_data as $key)
                                            { 
                                                $i++;

                                              ?>
                                            <tr>
                                                    <td><center><?php echo $i;?></center></td>   
                                                    <td><center><?php $user = $this->common_model->common_getRow('user',array('user_id'=>$key->user_id)); echo ucwords($user->full_name);?></center> </td>
                                                    <td><center><?php echo $key->car_name;?></center></td>
                                                    <td><center><?php echo $key->model;?></center></td>
                                                    <td><center><?php echo $key->color;?></center></td>
                                                    <td><center><?php  if($key->car_type == 1){echo "AC";}else{echo "Non AC";};?></center></td>
                                                    <td><center><?php echo $key->car_condition;?></center></td>
                                                    <td><center><?php echo $key->version;?></center></td>
                                                    <td><center><?php echo $key->year?></center></td>
                                                    <td><center><?php echo $key->mileage?></center></td> 
                                                    <td><center><?php echo $key->price?></center></td>
                                                    <td><center><?php echo $key->problem?></center></td>
                                                    <td><center><a href="<?php echo 'show/'.$key->work_id;?>"><span class="glyphicon glyphicon-picture" aria-hidden="true"></a></span></center></td>
                                                    <td><center><select style="" class="form-control" onchange="changestatus('<?php echo $key->work_id;?>',this.value)">
                                                    <option value="1"<?php if($key->status==1){ echo 'selected';} ?>> Disaprove </option>
                                                    <option value="0"<?php if($key->status==0){ echo 'selected';} ?>> Approve </option>
                                                    </select></center></td>
                                                    <!-- <td><center><a href="<?php echo 'edit/'.$this->common_model->id_encrypt($key->id);?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span></center></td>     -->
                                            </tr>
                                                <?php  
                                            } }
                                          else
                                          {?>
                                        <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
      <?php $this->load->view("admin/footer"); ?>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>

<script type="text/javascript">
        function changestatus(id,status)
        { 
            var str = "user_id="+id+"&admin_status="+status;
            //alert(str);
            var r = confirm('Are you really want to change status?');
            if(r==true)
            {
                $.ajax({
                  type:"POST",
                   url:"<?php echo base_url('garage_work/change_status')?>/",
                   data:str,
                   success:function(data)
                   {   //alert(data);
                      if(data==1000)
                       {
                            location.reload();
                       }
                   }
                });
            }location.reload();
        }
    </script>




     


