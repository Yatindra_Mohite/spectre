<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Edit Version
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Edit Version</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Edit Version</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('version_name/edit/'.$version_data->version_id)?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                

                <div class="form-body">
                    <div class="form-group">
                        <?php $brand = $this->db->query("SELECT * FROM model_name WHERE status = 1")->result();?>
                        <label class="control-label col-md-3">Model Name<span class="required"> * </span></label>
                        <div class="col-md-6">
                            <select class="form-control" name="model_id" required="">
                                        <option value="">Select Model name</option>
                                        <?php if(!empty($brand)){
                                                 foreach($brand as $key)
                                                 {?>
                                                 <option value="<?php echo $key->model_id;?>"<?php if($key->model_id == $version_data->model_id){ echo "selected";} ?>><?php echo $key->model_name;?></option>
                                                <?php } 
                                            } ?>
                                </select>
                        </div>        
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Add Version<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="text" placeholder="Version Name" value="<?php echo $version_data->version_name;?>" name="version_name" class="form-control" data-parsley-required-message="Version name is required"  required/>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" name="submit" value="Submit" >
                            <!-- <a href="<?php echo base_url()?>add_state"><button type="button" class="btn default">Cancel</button></a> -->
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>

<!-- <script type="text/javascript">
 $( function() {
    $( "#date1" ).datepicker({
       changeMonth: true,
       changeYear: true,
       dateFormat:'yy-mm-dd',
       minDate: 0 
    });
  } );
  </script> -->        
    </body>

</html>