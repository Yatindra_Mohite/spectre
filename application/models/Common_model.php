<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

	class Common_model extends CI_Model {
	public function __construct()
    {
        parent::__construct();
	}
	
	public function common_insert($tbl_name = false, $data_array = false)
	{
		$ins_data = $this->db->insert($tbl_name, $data_array);
		if($ins_data){
			return $last_id = $this->db->insert_id();
		}
		else{
			return false;
		}
	}

	public function updateData($table,$data,$where_array)
	{ 
	    $this->db->where($where_array);
		if($this->db->update($table,$data)){
			//print_r($this->db->last_query()); exit;
			return true;
		}
		else{
			//print_r($this->db->last_query()); exit;
			return false;
		}
	}

	// Function for select data
	public function getData($table,$where='', $order_by = false, $order = false, $join_array = false, $limit = false)
	{
		//$this->db->select('*');
		$this->db->from($table);

		if(!empty($where))
		{
			$this->db->where($where);
		}
		
		if(!empty($order_by))
		{
			$this->db->order_by($order_by, $order); 	
		}



		if(!empty($join_array))
		{
			foreach ($join_array as $key => $value) {

				$this->db->join($key, $value); 	
			}
			
		}

		if(!empty($limit))
		{
			$this->db->limit($limit); 	
		}

		$result = $this->db->get();
		

		//print_r($this->db->last_query()); exit;
		return $result->result();
		//return $result;
	}

	// Function for select data
	public function getDataField($field = false, $table, $where='', $order_by = false, $order = false, $join_array = false, $limit = false, $join_type = false )
	{
		$this->db->select($field);

		$this->db->from($table);

		if(!empty($where))
		{
			$this->db->where($where);
		}
		
		if(!empty($order_by))
		{
			$this->db->order_by($order_by, $order); 	
		}



		if(!empty($join_array))
		{
			foreach ($join_array as $key => $value) {

				if(!empty($join_type))
					$this->db->join($key, $value, 'left');
				else
					$this->db->join($key, $value); 	
			}
			
		}

		if(!empty($limit))
		{
			$this->db->limit($limit); 	
		}

		$result = $this->db->get();
		

		//print_r($this->db->last_query()); exit;
		return $result->result();
		//return $result;
	}

	public function common_getRow($tbl_name = flase, $where = false, $join_array = false)
	{
		$this->db->select('*');
		$this->db->from($tbl_name);
		
		if(isset($where) && !empty($where))
		{
			$this->db->where($where);	
		}
		
		if(!empty($join_array))
		{
			foreach($join_array as $key=>$value){
				$this->db->join($key,$value);
			}	
		}
		
		$query = $this->db->get();
		
		$data_array = $query->row();
		//print_r($this->db->last_query()); exit;
		if($data_array)
		{
			return $data_array;
		}
		else{
			return false;
		}
	}
	public function deleteData($table,$where)
	{ 
		$this->db->where($where);
		if($this->db->delete($table))
		{
			return true;
		}
		else{
			return false;
		}
	}
	
	public function sqlcount($table = false,$where = false)
	{
		$this->db->select('*');	
		$this->db->from($table); 
		if(isset($where) && !empty($where))
		{
			$this->db->where($where);	
		}
		//$this->db->limit($limit, $start);       
		$query = $this->db->get();
		//print_r($this->db->last_query()); exit;
		return $query->num_rows(); 
	}
	
	
	public function id_encrypt($str = false) // Id Encryption
	{
		$str=(56*$str)+111;
		return ($str);
	}
	
	public function id_decrypt($str = false) // Id Decryption
	{
		$str=($str-111)/56;
		return ($str);
	}
	
	public function id_encrypt_zub($str = false) // Id Encryption
	{
		$str=(56*$str)+111;
		return ($str);
	}
	
	public function id_decrypt_zub($str = false) // Id Decryption
	{
		$str=($str-111)/56;
		return ($str);
	}
	public function milliseconds() // Id Decryption
	{
		$str = round(microtime(true) * 1000);
		return ($str);
	}
	public function getAddress($lat, $lon)
	{
		$url  = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".$lat.",".$lon."&sensor=false";
		$json = @file_get_contents($url);
		$data = json_decode($json);
		$status = $data->status;
		$address = '';
		if($status == "OK")
		{
			$address = $data->results[0]->formatted_address;
		}
		return $address;
	}
	public function randomuniqueCode() 
	{
	    $alphabet = "123456789";
	    $pass = array(); /*remember to declare $pass as an array*/
	    $alphaLength = strlen($alphabet) - 1; /*put the length -1 in cache*/
	    for ($i = 0; $i < 6; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); /*turn the array into a string*/
	}

	function sendPushNotification($registration_ids, $message)
	{
	  
	   $url = 'https://fcm.googleapis.com/fcm/send';
	   $fields = array(
	       'registration_ids' => array($registration_ids),
	       'data' => $message,
	   );
	   $headers = array(
	       'Authorization:key=' . GOOGLE_API_KEY,
	       'Content-Type: application/json'
	   );

	   $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url); 
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	   // curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
	    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

	    $result = curl_exec($ch);
	    curl_close($ch);
	 
	   //return $result;
	  //echo $result; exit;
	}

	function sendNotification($registration_ids, $message)
	{
	  // echo '<pre>';	
	  // print_r($registration_ids);exit;
	   $url = 'https://fcm.googleapis.com/fcm/send';
	   $fields = array(
	       'registration_ids' => $registration_ids,
	       'data' => $message
	   );

	   // echo '<pre>';
	   // print_r($fields);exit;
	   $headers = array(
	       'Authorization:key=' . GOOGLE_API_KEY,
	       'Content-Type: application/json'
	   );

	   $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url); 
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	   // curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
	    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

	    $result = curl_exec($ch);
	    curl_close($ch);
	 
	   //return $result;
	  //$result; 
	}

	function encryptor_ym($action, $string) {
	    $output = false;

	    $encrypt_method = "AES-256-CBC";
	    //pls set your unique hashing key
	    $secret_key = 'muni';
	    $secret_iv = 'muni123';

	    // hash
	    $key = hash('sha256', $secret_key);

	    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
	    $iv = substr(hash('sha256', $secret_iv), 0, 16);

	    //do the encyption given text/string/number
	    if( $action == 'encrypt' ) {
	        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
	        $output = base64_encode($output);
	    }
	    else if( $action == 'decrypt' ){
	    	//decrypt the given text/string/number
	        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
	    }

	    return $output;
	}

}

?>