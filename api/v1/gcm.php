<?php
function AndroidNotification($device_token,$message)
{
  //print_r($message);
  $url = 'https://fcm.googleapis.com/fcm/send';
     $fields = array(
         'registration_ids' => array($device_token),
         'data' => $message,
     );
     $headers = array(
         'Authorization:key=' . GOOGLE_API_KEY,
         'Content-Type: application/json'
     );

     $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url); 
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
     // curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

      $result = curl_exec($ch);
      curl_close($ch);
      //print_r($result);exit;
}

function iOSPushNotification($deviceToken, $message ,$msg)
{ 
  $passphrase = "";
  $payload['aps'] = array(
    'alert' => $msg,
    'badge' => 1, 
    'type' => $message,
    'sound' => 'default'
  );   
  $payload = json_encode($payload);
  $apnsHost = 'gateway.push.apple.com';    
  $apnsPort = 2195;
  //$apnsCert = 'taxiappProcert.pem';
  $apnsCert = 'development_push_certificate.pem';
  $streamContext = stream_context_create();
  stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
  stream_context_set_option($streamContext, 'ssl', 'passphrase', $passphrase);
  $apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort,$error,$errorString,60,STREAM_CLIENT_CONNECT,$streamContext); 
  $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', $deviceToken) . chr(0) . chr(strlen($payload)) . $payload;
  fwrite($apns, $apnsMessage);
  @socket_close($apns);
  fclose($apns);
}

?>