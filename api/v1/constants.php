<?php

//Login
define('login_msg_en_1', 'Successfully Login.');
define('login_msg_ar_1', 'تسجيل الدخول بنجاح.');
define('login_msg_en_2', 'Invalid login credentials.');
define('login_msg_ar_2','اعتماد تسجيل الدخول غير صالح صالح.');
define('login_msg_en_3', 'Please check your email if you have already verification email OR click on resend button for email verification link.');
define('login_msg_ar_3', 'يرجى التحقق من بريدك الإلكتروني إذا كان لديك بريد إلكتروني للتحقق بالفعل أو انقر على زر إعادة إرسال رابط التحقق عبر البريد الإلكتروني.');
define('login_msg_en_5', 'Account does not exists.');
define('login_msg_ar_5', 'الحساب غير موجود.');
define('login_msg_en_6', 'Email & Password are required.');
define('login_msg_ar_6', 'مطلوب البريد الإلكتروني وكلمة المرور.');


//Common Function
define('admin_status_en', 'Your Spectre account has been temporarily suspended as a security precaution.');
define('admin_status_ar', 'تم تعليق حسابك في قلام مؤقتا كإجراء وقائي أمني.');
define('some_error_en', 'Something went wrong! please try again later.');
define('some_error_ar', 'هناك خطأ ما! الرجاء معاودة المحاولة في وقت لاحق.');
define('no_data_en', 'Data not available.'); 
define('no_data_ar', 'البيانات غير متوفرة');
define('no_request_en', 'No Request Parameter Found.');
define('invalid_request_en', 'Invalid request parameter.');

//Update Profile
define('update_profile_en_1', 'The file type you are attempting to upload is not allowed. Only jpg, jpeg and png file type allowed.');
define('update_profile_ar_1', 'نوع الملف الذي تحاول تحميله غير مسموح به. يسمح فقط بجبغ و جبيغ و ينغ نوع الملف.');
define('update_profile_en_2', 'Profile successfully updated.');
define('update_profile_ar_2', 'تم تحديث الملف الشخصي بنجاح.');
define('update_profile_en_3', 'Mobile number already registered.');
define('update_profile_ar_3', 'رقم الجوال مسجل بالفعل.');


//Signup
define('signup_en_1', 'Mobile number already registered please login.');
define('signup_ar_1', 'أنت مسجل بالفعل');
define('signup_en_2', 'Successfully registered and verification link has been sent to registered email address.');
define('signup_ar_2', 'تم إرسال رابط التسجيل والتحقق بنجاح إلى عنوان البريد الإلكتروني المسجل');
define('signup_en_3', 'Mobile number is already registered with other user type.');
define('signup_en_4', 'Successfully registered.');

//resend OTP
define('resent_en_1', 'OTP sent.');
define('resent_ar_1', 'تم إرسال رابط التحقق إلى عنوان البريد الإلكتروني المسجل');

//Forgot password
define('forgot_en_1', 'Reset password link has been send to registered email address.');
define('forgot_ar_1', 'تم إرسال رابط إعادة تعيين كلمة المرور إلى عنوان البريد الإلكتروني المسجل');

//Change Password
define('changed_en_1', 'Password successfully changed.');
define('changed_ar_1', 'تم تغيير كلمة المرور بنجاح');
define('changed_en_2', 'All fields are required.');
define('changed_ar_2', 'كل الحقول مطلوبة.');
define('changed_en_3', 'Old password does not match.');
define('changed_ar_3', 'كلمة المرور القديمة غير متطابقة');

//App version
define('version_en_1', 'We no longer support this version, Please update the application.');
define('version_ar_1', 'نحن لم تعد تدعم هذا الإصدار، يرجى تحديث التطبيق');

//post list
define('postlist_en_1', "Oops! No post available.Be the first to post here.");
define('postlist_ar_1', "وجه الفتاة! ليست هناك أية مشاركة متاحة. كن أول من نشر هنا.");
define('postlist_en_2', "Oops! No post available related to your interest.");
define('postlist_ar_2', "وجه الفتاة! لا توجد مشاركة متاحة ذات صلة باهتمامك.");

//Mobile
define('mobile_verifiy_en_1', "Mobile number has been verified successfully.");
define('mobile_verifiy_ar_1', "تم التحقق من رقم الجوال بنجاح");
define('mobile_verifiy_en_2', "OTP does not match.");
define('mobile_verifiy_ar_2', "لا يتطابق مكتب المدعي العام");
define('mobile_otp_en_1', "OTP has been sent to your registered mobile number.");
define('mobile_otp_ar_1', "تم إرسال مكتب المدعي العام إلى رقم هاتفك المحمول المسجل");

//forgot password otp
define('send_password_en_1', "Password has been change successfully.");
define('send_password_ar_1', "تم تغيير كلمة المرور بنجاح.");


//Rent
define('rent_add_en_1', "Rent successfully added.");
define('rent_add_en_2', "Data not available.");
define('rent_add_en_3', "Rent successfully updated.");
define('rent_add_en_4', "Successfully deleted.");
define('rent_add_en_5', "Rent list successfully get.");


//Post
define('post_add_en_1', "Post successfully added.");
define('post_add_en_2', "Post not available.");
define('post_add_en_3', "Post list successfully get.");
define('post_del_en_1', "Post successfully deleted.");
define('post_del_ar_1', "تم حذف المشاركة بنجاح.");
define('post_edit_en_1', "Post successfully updated.");
define('post_edit_ar_1', "تم تحديث المشاركة بنجاح");

//work
define('work_add_en_1', "Work successfully added.");
define('work_add_en_2', "Work list successfully get.");
define('work_add_en_3', "Work successfully updated.");
define('work_add_en_4', "Work successfully deleted.");

//other
define('carname_en', "list not available.");
define('interest_en', "Successfully interest.");
define('interest_en_1', "Already interested.");
define('rating_en_1', "Rating and review successfully submited.");
define('rating_en_2', "Rating and review list.");
define('garage_en', "Garage work list.");


//download file
define('download_en_1', "File not available");
define('download_ar_1', "الملف غير متاح");
define('download_en_2', "Oops! No file downloaded by you.");
define('download_ar_2', "وجه الفتاة! لم يتم تنزيل أي ملف من قبلك.");
//Notification
define('type_1', "added new post.");
define('type_2', "liked your post.");
define('type_3', "commented on your post.");
define('type_4', "followed you.");
define('type_5', "sent request for follow you.");
define('noti_list_en', "Oops! No notification available for you.");
define('noti_list_ar', "وجه الفتاة! لا يتوفر أي إشعار لك");






?>