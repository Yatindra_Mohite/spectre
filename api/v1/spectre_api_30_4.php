<?php
//die('king');
require '.././libs/Slim/Slim.php';
require_once 'dbHelper.php';
require_once 'auth.php';
require_once 'gcm.php';
require_once 'constants.php';
require 'PHPmailer/Send_Mail.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$app = \Slim\Slim::getInstance();
$db = new dbHelper();
//die('hello');

/*date_default_timezone_set("Asia/Kolkata");*/
$base_url = "https://spectrecar.com/spectre_app/";                                                                                                                               
$dateTime = date("Y-m-d H:i:s", time()); 
$militime=round(microtime(true) * 1000);
$path = '../../spectre_app/uploads/';
define('path', $path);
define('base_url', $base_url);
define('militime', $militime);
define('dateTime', $dateTime);
define('GOOGLE_API_KEY', 'AAAAPNefmls:APA91bHx0bceP0JwUfJQqGCNPh4KdVZGAwZF6E9x4NOPZFEADSGg3zr76CA3PwvWFQ4r7bCSzMPogzSFsGGQSZv52ed2dBDmu5ZcJ5KAhQhrLBotQ2fY2V1m2mf3exAYBjdA-OO1pzvu');

$app->post('/test',function() use ($app){
sms_send('918109059062','Hello from spectre app testing');

});

//echo constant('some_error_'.$aa); exit;
//define('user_image_url',$user_image_url);
// $aa = 'some_error_'.$aa;
// echo $aa; exit;
//die('hello');
// $app->post('/notification_test',function() use ($app){
// 	// $deviceToken = '0C7643FFDCBF3B84BD3849E0CF5E5E3458939522027E450A60522490CFB50CC1';
// 	// $title = 'New interest';
// 	// $msg = 'You have received interest from IOS';
// 	// $message = array('msg'=>$msg,'interested_id'=>0,'receiver_id'=>0,'type'=>1);
//  //    iOSPushNotification($deviceToken, $message ,$title);	
//     Send_Mail('noreply@spectrecar.com','yatindra.mohite@ebabu.co',"Testing for smtp email",'I have received spectre app email');
//     //sms_send('9617822421','hiiii jaipal');
  
// });

$app->post('/signup',function() use ($app){

  $json1 = file_get_contents('php://input');
  if(!empty($json1))
  {
    $data = json_decode($json1);
    $email= $data->user_email;
    $user_type= $data->user_type; /// 1 for user //// 2 for garage
    $full_name= ucwords($data->user_name);
    $mobile = $data->user_mobile;
    $password = $data->user_password;
    $mobile_code = $data->mobile_code;
    $language = $data->language;
    global $db;

    if(!empty($mobile) && !empty($password) && !empty($mobile_code) && !empty($full_name) && !empty($email) && !empty($user_type) && !empty($language)) 
    { 
        $query_login = $db->select("user","*",array('mobile_code'=>$mobile_code,'mobile_no'=>$mobile));
        if($query_login["status"] == "success")
        {
          if($query_login['data'][0]['type'] == $user_type) {

            if($query_login['data'][0]['admin_status'] == 0)
            {
	            if($query_login['data'][0]['mobile_status'] == 1)
	            {
	              $query_login['status'] ="failed";
	              $query_login['message'] = constant('signup_'.$language.'_1');
	              unset($query_login['data']);
	              echoResponse(200,$query_login);
	            }else{
	              // $Otp = 1234;
	              // $update = $db->update("user",array('otp_code'=>hash('sha256', $Otp),'full_name'=>$full_name,'password'=>sha1($password)),array('user_id'=>$query_login['data'][0]['user_id']),array());
	              $query_login['status'] ="failed";
	              $query_login['message'] = constant('signup_'.$language.'_1');
	              unset($query_login['data']);
	              echoResponse(200,$query_login);
	            }
            }else
            {
            	$query_login['status'] ="failed";
              	$query_login['message'] = constant('admin_status_'.$language);
              	unset($query_login['data']);
              	echoResponse(200,$query_login);
            }
          }else
          {
              $query_login['status'] ="failed";
              $query_login['message'] = constant('signup_'.$language.'_3');
              unset($query_login['data']);
              echoResponse(200,$query_login);
          }
        }else
        {
          $code = randomTxn();
          $Otp = "OTP for spectre app is: ".$code;
          $user_data = array(
                    'full_name'=>$full_name,
                    'mobile_code'=>$mobile_code,
                    'mobile_no'=>$mobile,
                    'password'=>sha1($password),
                    'otp_code'=>hash('sha256', $code),
                    'email'=>$email,
                    'type'=>$user_type,
                    'create_at'=>militime
                  );
          	$rows2 = $db->insert("user",$user_data,array());
            if($rows2['status']=="success")
            { 
                sms_send($mobile_code.$mobile,$Otp);
                $rows2['status'] ="success";
                $rows2['message'] = constant('signup_'.$language.'_4');
                echoResponse(200,$rows2);
            }else
            {
               $rows2['status'] ="1000";
               $rows2['message'] = constant('some_error_'.$language);
               unset($rows2['data']);
               echoResponse(200,$rows2);
            }
        }
    }else{
       $json2['status'] ="failed";
       $json2['message'] = constant('invalid_request_'.$language);
       echoResponse(200,$json2);
    }
  }else
  {
      $json1['status'] ="failed";
      $json1['message'] = "No Request Parameter Found.";
      echoResponse(200,$json1);
  }
});

$app->post('/resend_otp',function() use ($app){

  $json1 = file_get_contents('php://input');
  if(!empty($json1))
  {
    $data = json_decode($json1);
    
    $mobile = $data->user_mobile;
    $language = $data->language;
    global $db;
        if(!empty($mobile) && !empty($language)) 
	    { 
	        $query_login = $db->select("user","*",array('mobile_no'=>$mobile));
	        if($query_login["status"] == "success")
	        {
	           if($query_login['data'][0]['admin_status'] == 0)
	           {
		           //$otp = 1234;
               $code = randomTxn();
               $otp = "OTP for spectre app is: ".$code;
               $update = $db->update("user",array('otp_code'=>hash('sha256', $code)),array('user_id'=>$query_login['data'][0]['user_id']),array());
               sms_send($query_login['data'][0]['mobile_code'].$mobile,$otp);
		           $query_login['status'] ="success";
		           $query_login['message'] = constant('resent_'.$language.'_1');
		           unset($query_login['data']);
		           echoResponse(200,$query_login);
	           }else
	           {
		           $query_login['status'] ="1000";
		           $query_login['message'] = constant('admin_status_'.$language);
		           unset($query_login['data']);
		           echoResponse(200,$query_login);  
	           }
	        }else
	        {
	           $query_login['status'] ="failed";
	           $query_login['message'] = constant('login_msg_'.$language.'_5');
	           unset($query_login['data']);
	           echoResponse(200,$query_login);  
	        }
	    }else{
	       $json2['status'] ="failed";
	       $json2['message'] = constant('invalid_request_'.$language);
	       echoResponse(200,$json2);
	    }
  	}else
  	{
      $json1['status'] ="failed";
      $json1['message'] = "No Request Parameter Found.";
      echoResponse(200,$json1);
  	}
});

$app->post('/login_with_otp',function() use ($app){

  $json1 = file_get_contents('php://input');
  if(!empty($json1))
  {
    $data = json_decode($json1);
    
    $language = $data->language;
    $mobile = $data->user_mobile;
    $otp = $data->otp;
    $otp = hash('sha256', $otp);
    $device_type= $data->user_device_type;
    $device_id = $data->user_device_id;
    $device_token = $data->user_device_token;
    $token = bin2hex(openssl_random_pseudo_bytes(16));
    $token = $token.militime;
    $images = $garage_image = '';

    global $db;

    if(!empty($mobile) && !empty($otp) && !empty($device_type) && !empty($device_id) && !empty($device_token) && !empty($language)) 
    { 
        $query_login = $db->select("user","*",array('mobile_no'=>$mobile));
        if($query_login["status"] == "success")
        {
           	if($query_login['data'][0]['admin_status']==0)
           	{
	           	if ($otp == $query_login['data'][0]['otp_code']) {

	             $query_update = $db->update("user",array('token'=>$token,'device_type'=>$device_type,'device_token'=>$device_token,'device_id'=>$device_id,'otp_code'=>'','mobile_status'=>1,'update_at'=>militime),array('user_id'=>$query_login['data'][0]['user_id']),array());

	           		if ($query_update['status'] == 'success') {

	            		if(!empty($query_login['data'][0]['image']))
	                  	{
	                      $images = base_url.'uploads/user_image/'.$query_login['data'][0]['image'];
	                  	}
	                  	if(!empty($query_login['data'][0]['garage_image']))
	                  	{
	                      $garage_image = base_url.'uploads/garage_image/'.$query_login['data'][0]['garage_image'];
	                  	}
	                	$type = $query_login['data'][0]['type'];

		                if (empty($query_login['data'][0]['address'])) {
		                 $add  = "";
		                }else{
		                 $add = $query_login['data'][0]['address'];
		                }

	               		//var_dump($type); exit;
	            	$arr = array(
	                   'user_id'=>$query_login['data'][0]['user_id'],
	                   'user_name'=>$query_login['data'][0]['full_name'],  
	                   'mobile_code'=>$query_login['data'][0]['mobile_code'],
	                   'user_mobile'=>$query_login['data'][0]['mobile_no'],
	                   'user_email'=>$query_login['data'][0]['email'],
	                   'user_address'=>$add,
	                   'user_type'=>$type,
	                   'service_type'=>$query_login['data'][0]['service_type'],
	                   'user_image'=>$images,
	                   'garage_image'=>$garage_image,
	                   'user_token'=>$token,
	                   'expertise'=>$query_login['data'][0]['expertise'],
	                   'car_repaire'=>$query_login['data'][0]['car_repaire']
	                );
	                                        
	                 $query_update['status'] ="success";
	                 $query_update['message'] = constant('login_msg_'.$language.'_1');
	                 $query_update['data'] = $arr;
	                 echoResponse(200,$query_update);
	             }else
	             {
	                $query_update['status'] ="failed";
	                $query_update['message'] = constant('some_error_'.$language);
	                unset($query_update['data']);
	                echoResponse(200,$query_update);
	             }

	           	}else
	           	{
	              $query_login['status'] ="failed";
	              $query_login['message'] = constant('mobile_verifiy_'.$language.'_2');
	              unset($query_login['data']);
	              echoResponse(200,$query_login); 
	           	}
           	}else
           	{
           		$json2['status'] ="failed";
       			$json2['message'] = constant('admin_status_'.$language);
       			echoResponse(200,$json2);
           	}
        }else
        {
           $query_login['status'] ="failed";
           $query_login['message'] = constant('login_msg_'.$language.'_5');
           unset($query_login['data']);
           echoResponse(200,$query_login);  
        }
    }else{
       $json2['status'] ="failed";
       $json2['message'] = constant('invalid_request_'.$language);
       echoResponse(200,$json2);
    }
  }else
  {
      $json1['status'] ="failed";
      $json1['message'] ="No Request parameter";
      echoResponse(200,$json1);
  }
});

$app->post('/login',function() use ($app){

  $json1 = file_get_contents('php://input');
  if(!empty($json1))
  {
    $data = json_decode($json1);
    $language = $data->language;
    $mobile = $data->user_mobile;
    $password = $data->user_password;
    $password = sha1($password);
    $device_type= $data->user_device_type;
    $device_id = $data->user_device_id;
    $device_token = $data->user_device_token;
    $token = bin2hex(openssl_random_pseudo_bytes(16));
    $token = $token.militime;
    $images = $garage_image = '';

    global $db;

    if(!empty($mobile) && !empty($password) && !empty($language)) 
    { 
        $query_login = $db->select("user","*",array('mobile_no'=>$mobile));
        if($query_login["status"] == "success")
        {
          if($query_login['data'][0]['admin_status'] == 0)
          {
            if($query_login['data'][0]['mobile_status'] == 1)
            {

             if ($password == $query_login['data'][0]['password']) {

               $query_update = $db->update("user",array('token'=>$token,'device_type'=>$device_type,'device_token'=>$device_token,'device_id'=>$device_id,'update_at'=>militime),array('user_id'=>$query_login['data'][0]['user_id']),array());
               
               if ($query_update['status'] == 'success') {

              if(!empty($query_login['data'][0]['image']))
                    {
                        $images = base_url.'uploads/user_image/'.$query_login['data'][0]['image'];
                    }
                    if(!empty($query_login['data'][0]['garage_image']))
                    {
                        $garage_image = base_url.'uploads/garage_image/'.$query_login['data'][0]['garage_image'];
                    }
                   $type = $query_login['data'][0]['type'];

                   if (empty($query_login['data'][0]['address'])) {
                    $add  = "";
                   }else{
                    $add = $query_login['data'][0]['address'];
                   }
                   // $a = (string)$type;
                   // $b = "$a";
                   //var_dump($type); //exit;
                   //$i = settype($query_login['data'][0]['type'], "string");
                  // $str = <<<EOD $type EOD; 
              $arr = array(
                   'user_id'=>$query_login['data'][0]['user_id'], 
                   'user_name'=>$query_login['data'][0]['full_name'],  
                   'mobile_code'=>$query_login['data'][0]['mobile_code'],
                   'user_mobile'=>$query_login['data'][0]['mobile_no'],
                   'user_email'=>$query_login['data'][0]['email'],
                   'user_address'=>$add,
                   'user_type'=>$type,
                   'service_type'=>$query_login['data'][0]['service_type'],
                   'user_image'=>$images,
                   'garage_image'=>$garage_image,
                   'user_token'=>$token,
                   'expertise'=>$query_login['data'][0]['expertise'],
                   'car_repaire'=>$query_login['data'][0]['car_repaire']
                   );
                                          
                   $query_update['status'] ="success";
                   $query_update['message'] ="Successfully login";
                   $query_update['data'] = $arr;
                   echoResponse(200,$query_update);
               }else
               {
                  $query_update['status'] ="failed";
                  $query_update['message'] ="Something went wrong. Try again later";
                  unset($query_update['data']);
                  echoResponse(200,$query_update);
               }
             }else
             {
                $query_login['status'] ="failed";
                $query_login['message'] = constant('login_msg_'.$language.'_2');
                unset($query_login['data']);
                echoResponse(200,$query_login); 
             }
          	}
          	else
          	{
              //$Otp = 1234;
              $Otp = randomTxn();
              $otp = "OTP for spectre app is: ".$Otp;
              $update = $db->update("user",array('otp_code'=>hash('sha256', $Otp)),array('user_id'=>$query_login['data'][0]['user_id']),array());
              sms_send($query_login['data'][0]['mobile_code'].$mobile,$otp);
	            $query_login['status'] ="failed";
	            $query_login['message'] = constant('resent_'.$language.'_1');
	            unset($query_login['data']);
	            echoResponse(200,$query_login);
           	}
          }else
          {
            $query_login['status'] = "failed";
            $query_login['message'] = constant('admin_status_'.$language);
            unset($query_login['data']);
            echoResponse(200,$query_login);
          }
        }else
        {
           $query_login['status'] ="failed";
           $query_login['message'] = constant('login_msg_'.$language.'_5');
           unset($query_login['data']);
           echoResponse(200,$query_login);  
        }
    }else{
       $json2['status'] ="failed";
       $json2['message'] = constant('invalid_request_'.$language);
       echoResponse(200,$json2);
    }
  }else
  {
      $json1['status'] ="failed";
      $json1['message'] ="No Request parameter";
      echoResponse(200,$json1);
  }
});

$app->post('/update_profile',function() use ($app){
$headers = get_header_token();
if(!empty($headers['secret_key']))
{
  	$check = token_auth($headers['secret_key']);
  	if($check['status']=="true")
   	{
        $json1 = file_get_contents('php://input');
		$data = json_decode($json1);
      	$language = $data->language;
      	if($check['data'][0]['admin_status'] == 0) 
      	{
	        if(!empty($json1))
	        {
	           $name = $data->user_name;
	           $mobile = $data->user_mobile; 
	           $expertise = $data->expertise;
	           $car_repaire = $data->car_repaire;
	           $email = $data->user_email;
	           $address = $data->address;
	           $image = $data->image;
	           $garage_image = $data->garage_image;
	           $service_type = $data->service_type; //1 = maintainance ,2= modification
	         //print_r($app->request()->params());
	         //exit;
	         	global $db;
	         
	         	if(!empty($name) && !empty($mobile) && !empty($language)){

	          // if ($mobile == $check['data'][0]['mobile_no'])
	          // {
	               if(!empty($image))
	               {  
	                  $image_name = 'img1_'.militime.$check['data'][0]['user_id'].'.jpeg';
	                  //exit;
	                   $path = path.'user_image/'.$image_name;
	                  
	                  $base64img = str_replace('data:image/jpeg;base64,', '', $image);
	                  
	                  $data = base64_decode($base64img);
	                  
	                  $aa = file_put_contents($path, $data);
	                  //$image_name= $_FILES['image']['name'];
	                  //$image_name = militime.$image_name;
	                  //move_uploaded_file($image,"../../uploads/user_image/".$image_name);
	                  // $u_image1 = base_url."uploads/user_image/".$image_name;
	               }
	               else
	               {
	                  if ($check['data'][0]['image'] == '') {
	                    $image_name = '';
	                  }else{
	                    $image_name = $check['data'][0]['image'];
	                  }
	                 
	               }
	               if(!empty($garage_image))
	               {  
	                    $g_image_name = 'garage_image_'.militime.$check['data'][0]['user_id'].'.jpeg';
	                    //exit;
	                    $path = path.'garage_image/'.$g_image_name;
	                  
	                    $base64img = str_replace('data:image/jpeg;base64,', '', $garage_image);
	                  
	                    $data = base64_decode($base64img);
	                  
	                    $aa = file_put_contents($path, $data);
	                }else
	                {
	                    if ($check['data'][0]['garage_image'] == '') {
	                      $g_image_name = '';
	                    }else{
	                      $g_image_name = $check['data'][0]['garage_image'];
	                    }
	                }
	                $user_data = array(
	                      'full_name'=>$name,
	                      'mobile_no'=>$mobile,
	                      'email'=>$email,
	                      'image'=>$image_name,
	                      'address'=>$address,
	                      'expertise'=>$expertise,
	                      'car_repaire'=>$car_repaire,
	                      'garage_image'=>$g_image_name,
	                      'service_type'=>$service_type,
	                      'update_at'=>militime
	                    );
	              $rows2 = $db->update("user",$user_data,array('user_id'=>$check['data'][0]['user_id']),array());
	              if($rows2['status']=="success")
	              {    
	                  $check_image = $db->select("user","*",array("user_id"=>$check['data'][0]['user_id']));
	                  if(!empty($check_image['data'][0]['image']))
	                  { 
	                    $image_name = base_url.'uploads/user_image/'.$check_image['data'][0]['image'];
	                  }else{                   
	                    $image_name = '';
	                  }
	                  if(!empty($check_image['data'][0]['garage_image']))
	                  { 
	                    $gimage_name = base_url.'uploads/garage_image/'.$check_image['data'][0]['garage_image'];
	                  }else{                   
	                    $gimage_name = '';
	                  }
	                  $car_rep = '';
	                  $exp = '';
	                  if (!empty($check_image['data'][0]['expertise'])) {
	                        $exp = $check_image['data'][0]['expertise'];
	                       }
	                  if (!empty($check_image['data'][0]['car_repaire'])) {
	                        $car_rep = $check_image['data'][0]['car_repaire'];
	                      }          
	                  $arr = array(
	                         'user_id'=>$check['data'][0]['user_id'],
	                         'user_name'=>$check_image['data'][0]['full_name'],  
	                         'mobile_code'=>$check_image['data'][0]['mobile_code'],
	                         'user_mobile'=>$check_image['data'][0]['mobile_no'],
	                         'user_email'=>$check_image['data'][0]['email'],
	                         'user_address'=>$check_image['data'][0]['address'],
	                         'user_type'=>$check_image['data'][0]['type'],
	                         'service_type'=>$check_image['data'][0]['service_type'],
	                         'expertise'=>$exp,
	                         'car_repaire'=>$car_rep,
	                         'user_image'=>$image_name,
	                         'garage_image'=>$gimage_name,
	                         'user_token'=>$check_image['data'][0]['token'],
	                    );
	                  
	                  $rows2['status'] ="success";
	                  $rows2['message'] = constant('update_profile_'.$language.'_2');
	                  $rows2['data'] = $arr;
	                  //unset($rows2['data']);  
	                  echoResponse(200,$rows2);
	              }else
	              {
	                 $rows2['status'] ="failed";
	                 $rows2['message'] = constant('some_error_'.$language);
	                 unset($rows2['data']);
	                 echoResponse(200,$rows2);
	              }
			          // }else
			          // {
			          //     $query_login = $db->select("user","*",array('mobile_no'=>$mobile,'mobile_status'=>1));
			          //     if($query_login["status"] == "success")
			          //     {
			          //        $query_login['status'] ="failed";
			          //        $query_login['message'] ="mobile number already registered";
			          //        unset($query_login['data']);
			          //        echoResponse(200,$query_login);
			          //     }else{

			          //       $Otp = substr(randomTxn(),0,4);
			          //       $msg ="Your OTP for Food Triangle is:".$Otp;
			          //       //sms_send($mobile,$msg);
			          //       $update = $db->update("user",array('otp_code'=>hash('sha256', '1234'),'update_at'=>militime),array('user_id'=>$check['data'][0]['user_id']),array());
			          //       $query_login['status'] ="failed";
			          //       $query_login['message'] ="Otp send Successfully.";
			          //       unset($query_login['data']);
			          //       echoResponse(200,$query_login);
			          //     }       
			          // }           
	         	}else
	         	{
		           $json2["status"] = "failed";
		           $json2['message'] = constant('invalid_request_'.$language);
		           echoResponse(200,$json2);
	         	}
	        }
	        else{
	           $json1["status"] = "failed";
	           $json1['message'] ="No Request parameter";
	           echoResponse(200,$json1);
	        }
      	}
      	else
      	{
	        $check['status'] = "1000";
	        $check['message'] = constant('admin_status_'.$language);
	        unset($check['data']);
	        echoResponse(200,$check);
      	}       
    }
    else
    {
      $check['status'] = "false";
      $check['message'] = "Invalid Token";
      unset($check['data']);
      echoResponse(200,$check);
    }
}
else
{
  $check['status'] = "false";
  $check['message'] = "Unauthorised access";
  unset($check['data']);
  echoResponse(200,$check);
} 
});

// $app->post('/update_profile_otp',function() use ($app){
// $headers = get_header_token();
// if(!empty($headers['secret_key']))
// {
//   $check = token_auth($headers['secret_key']);
//   if($check['status']=="true")
//    {
//       if($check['data'][0]['admin_status'] == 0) 
//       { 
//         $json1 = file_get_contents('php://input');
//         if(!empty($json1))
//         {

//          $name = $app->request()->params('user_name');
//          $mobile = $app->request()->params('user_mobile');
//          $email = $app->request()->params('user_email');
//          $address = $app->request()->params('address');
//          $user_otp = $app->request()->params('otp');
//          $user_otp = hash('sha256', $user_otp);
//          global $db;

//          if(!empty($name) && !empty($mobile)){

//           if ($user_otp == $check['data'][0]['otp_code'])
//           {
//              if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']))
//                {
//                       $image= $_FILES['image']['tmp_name'];
//                       $image_name= $_FILES['image']['name'];
//                       $image_name = militime.$image_name;
//                       move_uploaded_file($image,"../../uploads/user_image/".$image_name);
//                      // $u_image1 = base_url."uploads/user_image/".$image_name;
//                }
//                else
//                {
//                   if ($check['data'][0]['image'] == '') {
//                     $image_name = '';
//                   }else{
//                     $image_name = $check['data'][0]['image'];
//                   }
                 
//                }

//                $user_data = array(
                      
//                       'full_name'=>$name,
//                       'mobile_no'=>$mobile,
//                       'email'=>$email,
//                       'image'=>$image_name,
//                       'address'=>$address,
//                       'update_at'=>militime
//                     );
//               print_r($user_data);

//               $rows2 = $db->update("user",$user_data,array('user_id'=>$check['data'][0]['user_id']),array());
//               if($rows2['status']=="success")
//               {    
//                   $check_image = $db->select("user","image",array("user_id"=>$check['data'][0]['user_id']));
//                   if(!empty($check_image['data'][0]['image']))
//                   { 
//                     $image_name = base_url.'uploads/user_image/'.$check_image['data'][0]['image'];
//                   }else{                   
//                     $image_name = '';
//                   }             

//                   $rows2['status'] ="success";
//                   $rows2['message'] ="Profile update successfully";
//                   $rows2['user_image'] =$image_name;
//                   unset($rows2['data']);  
//                   echoResponse(200,$rows2);
//               }else
//               {
//                  $rows2['status'] ="failed";
//                  $rows2['message'] ="something went wrong! Please try again later.";
//                  unset($rows2['data']);
//                  echoResponse(200,$rows2);
//               }
//           }else
//           {
//               $check['status'] = "failed";
//               $check['message'] = "Otp not matched";
//               unset($check['data']);
//               echoResponse(200,$check);
//           }           
//          }else
//          {
//            $json2["status"] = "failed";
//            $json2['message'] ="Request parameter not valid";
//            echoResponse(200,$json2);
//          }
//          }else
//          {
//            $json2["status"] = "failed";
//            $json2['message'] ="No Request parameter";
//            echoResponse(200,$json2);
//          }
//       }
//       else
//       {
//         $check['status'] = "failed";
//         $check['message'] = "Your Food spectre and has been temporarily suspended as a security precaution.";
//         unset($check['data']);
//         echoResponse(200,$check);
//       }       
//     }
//     else
//     {
//       $check['status'] = "false";
//       $check['message'] = "Invalid Token";
//       unset($check['data']);
//       echoResponse(200,$check);
//     }
// }
// else
// {
//   $check['status'] = "false";
//   $check['message'] = "Unauthorised access";
//   unset($check['data']);
//   echoResponse(200,$check);
// } 
// });

$app->post('/forgot_password',function() use ($app){

  $json1 = file_get_contents('php://input');
  if(!empty($json1))
  {
    $data = json_decode($json1);
    
    $language = $data->language;
    $mobile = $data->user_mobile;
    $type = $data->type;  //1 mobile , 2 = email
    //$otp = substr(randomTxn(),0,4);
    $otp = randomTxn();;
    if($type==1)
    	$condition = array('mobile_no'=>$mobile);
    else
    	$condition = array('email'=>$mobile);

    global $db;

    if(!empty($mobile)) 
    { 
        $query_login = $db->select("user","*",$condition);
        if($query_login["status"] == "success")
        {
          	if($query_login['data'][0]['admin_status'] == 0)
          	{
	            if($type == 1)
	            {
	              	//$msg ="Your OTP for spectre is:".$otp;
	              //sms_send($mobile,$msg);
	              $Otp = "OTP for spectre app is: ".$otp;
                sms_send($query_login['data'][0]['mobile_code'].$mobile,$Otp);
              	}else{
    				        $message = 'Reset Password OTP: '.$otp;
                  	$subject = "Spectre App: Forgot Password";
                  	$email_from ='info@spectrecar.com';
                  	$cc = '';
                  	Send_Mail($email_from,$mobile,$subject,$message);	
              }
  		        $update = $db->update("user",array('otp_code'=>hash('sha256', $otp),'update_at'=>militime),array('user_id'=>$query_login['data'][0]['user_id']),array());
  		        $query_login['status'] ="success";
  		        $query_login['message'] = constant('resent_'.$language.'_1');
  		        unset($query_login['data']);
  		        echoResponse(200,$query_login);
	        }else{
	            $query_login['status'] = "failed";
	            $query_login['message'] = constant('admin_status_'.$language);
	            unset($query_login['data']);
	            echoResponse(200,$query_login);
	        }      
        }else
        {
          //if($type ==1){ $msg = 'Invalid mobile number.'; }else{ $msg = 'Email not registered'; }
           $query_login['status'] ="failed";
           $query_login['message'] = constant('login_msg_'.$language.'_5');
           unset($query_login['data']);
           echoResponse(200,$query_login);  
        }
    }else{
       $json2['status'] ="failed";
       $json2['message'] = constant('invalid_request_'.$language);
       echoResponse(200,$json2);
    }
  }else
  {
      $json1['status'] ="failed";
      $json1['message'] ="No Request parameter";
      echoResponse(200,$json1);
  }
});

$app->post('/forgot_password_mobile',function() use ($app){

  $json1 = file_get_contents('php://input');
  if(!empty($json1))
  {
    $data = json_decode($json1);
    
    $language = $data->language;
    $mobile = $data->user_mobile;
    $otp = $data->otp;
    $otp = hash('sha256', $otp);
    $password= $data->user_password;
    $device_type= $data->user_device_type;
    $device_id = $data->user_device_id;
    $device_token = $data->user_device_token;
    $type = $data->type;
    $token = bin2hex(openssl_random_pseudo_bytes(16));
    $token = $token.militime;
    global $db;

    if(!empty($mobile) && !empty($otp) && !empty($password)) 
    { 
	    if($type==1)
	    	$condition = array('mobile_no'=>$mobile);
	    else
	    	$condition = array('email'=>$mobile);
	    
        $query_login = $db->select("user","*",$condition);
        if($query_login["status"] == "success")
        {
            if($query_login['data'][0]['admin_status'] == 0)
            {
             	if ($otp == $query_login['data'][0]['otp_code']) {

             	$query_update = $db->update("user",array('token'=>$token,'password'=>sha1($password),'device_type'=>$device_type,'device_token'=>$device_token,'device_id'=>$device_id,'otp_code'=>'','mobile_status'=>1,'update_at'=>militime),array('user_id'=>$query_login['data'][0]['user_id']),array());

             	if ($query_update['status'] == 'success') 
             	{
                    if(!empty($query_login['data'][0]['image']))
                    {
                        $images = base_url.'uploads/user_image/'.$query_login['data'][0]['image'];
                    }else
                    {
                      $images = '';
                    }

                    if(!empty($query_login['data'][0]['garage_image']))
                    {
                        $garage_image = base_url.'uploads/garage_image/'.$query_login['data'][0]['garage_image'];
                    }else
                    {
                      $garage_image = '';
                    }

                    if (empty($query_login['data'][0]['address'])) {
                     $add  = "";
                    }else{
                     $add = $query_login['data'][0]['address'];
                    }

                    $arr = array(
                   'user_id'=>$query_login['data'][0]['user_id'], 
                   'user_name'=>$query_login['data'][0]['full_name'],  
                   'mobile_code'=>$query_login['data'][0]['mobile_code'],
                   'user_mobile'=>$query_login['data'][0]['mobile_no'],
                   'user_email'=>$query_login['data'][0]['email'],
                   'user_address'=>$add,
                   'user_type'=>$query_login['data'][0]['type'],
                   'service_type'=>$query_login['data'][0]['service_type'],
                   'user_image'=>$images,
                   'garage_image'=>$garage_image,
                   'user_token'=>$token,  
                   );
                                          
                   $query_update['status'] = "success";
                   $query_update['message'] = constant('changed_'.$language.'_1');
                   $query_update['data'] = $arr;
                   echoResponse(200,$query_update);                
               }else
               {
                  $query_update['status'] ="failed";
                  $query_update['message'] = constant('some_error_'.$language);
                  unset($query_update['data']);
                  echoResponse(200,$query_update);
               }

             }else
             {
                $query_login['status'] ="failed";
                $query_login['message'] = constant('mobile_verifiy_'.$language.'_2');
                unset($query_login['data']);
                echoResponse(200,$query_login); 
             }
          	}else{
          		$query_login['status'] = "failed";
              	$query_login['message'] = constant('admin_status_'.$language);
              	unset($query_login['data']);
              	echoResponse(200,$query_login);
        	}   
        }else
        {
           $query_login['status'] ="failed";
           $query_login['message'] = constant('login_msg_'.$language.'_5');
           unset($query_login['data']);
           echoResponse(200,$query_login);  
        }
    }else{
       $json2['status'] ="failed";
       $json2['message'] = constant('invalid_request_'.$language);
       echoResponse(200,$json2);
    }
  }else
  {
      $json1['status'] ="failed";
      $json1['message'] ="No Request parameter";
      echoResponse(200,$json1);
  }
});

$app->post('/add_post',function() use ($app){
  $headers = get_header_token();
  if(!empty($headers['secret_key']))
  {
    $check = token_auth($headers['secret_key']);
    if($check['status']=="true")
    {
        $json1 = file_get_contents('php://input');
        if(!empty($json1))
        {
    		$data = json_decode($json1);
    	    $language = $data->language;
      		if($check['data'][0]['admin_status'] == 0)
      		{ 
	        	// print_r($data);

	         	// ( [car_name] => woooo [model] => 1 [version] => 456 [year] => 1596 [mileage] => 12 [price] => 250 [colour] => red [car_type] => 1 [car_condition] => good [image] => ssdsdsdsdsd ) 

	         	//exit;
		         $car_name = $data->car_name;
		         $car_name_id = $data->car_name_id;
		         $model = $data->model;
		         $model_id = $data->model_id;
		         $version = $data->version;
		         $version_id = $data->version_id;
		         $year = $data->year;
		         $mileage = $data->mileage; 
		         $price = $data->price;
		         $colour = $data->colour;
		         $car_type = $data->car_type;
		         $car_condition = $data->car_condition;
		         $image = $data->image; 

	        	$user_id =  $check['data'][0]['user_id'];
         
	        	if(!empty($car_name) && !empty($car_name_id) && !empty($model) && !empty($model_id) && !empty($version) && !empty($version_id) && !empty($year) && !empty($mileage) && !empty($price) && !empty($colour) && !empty($car_type) && !empty($car_condition))
	         	{ 
	         	
	          		$data = array(
	                       'car_name'=>$car_name,
	                       'car_name_id'=>$car_name_id,
	                       'model' => $model,
	                       'model_id' => $model_id,
	                       'version'=>$version,
	                       'version_id'=>$version_id,
	                       'color' => $colour,
	                       'car_condition' => $car_condition,
	                       'car_type'=>$car_type,
	                       'year' => $year,
	                       'mileage'=>$mileage,
	                       'price'=>$price, 
	                       'user_id'=>$user_id,
	                       'status'=>1,
	                       'create_at'=>militime
	                       );

	              global $db;  
	              $rowss = $db->insert("add_post",$data,array());
	              

	              if($rowss['status']=='success')
	              {
	                 //$image = $_FILES["image"]["name"];

	                  for($i=0; $i<count($image); $i++)
	                  { 
	                    //print_r($image[0]);

	                    // $image1 = md5(militime.$image[$i]).'.png';
	                    // //print_r($image1);
	                    // move_uploaded_file($_FILES["image"]["tmp_name"][$i],"../../uploads/all_post/".$image1);
	                    $image_name = 'img'.$i.'_'.militime.$check['data'][0]['user_id'].'.jpeg';
	                    //exit;
	                    $path = path.'all_post/'.$image_name;
	                  
	                    $base64img = str_replace('data:image/jpeg;base64,', '', $image[$i]);
	                  
	                    $data = base64_decode($base64img);
	                  
	                    $aa = file_put_contents($path, $data);

	                    $ins_img = $db->insert('car_image',array('mix_id'=>$rowss['data'],'user_id'=>$user_id,'image'=>$image_name,'type'=>1),array());
	                    $img_arr[] = base_url.'uploads/all_post/'.$image_name;
	                  }

	                  $rowss["status"] = "success"; 
	                  $rowss["message"] = constant('post_add_'.$language.'_1');
	                  echoResponse(200, $rowss);
	              }
	              else
	              {
	                  $rowss["status"] = "failed"; 
	                  $rowss["message"] = constant('some_error_'.$language);
	                  unset($rowss['data']);
	                  echoResponse(200, $rowss);
	              } 
		        }
		        else
		        { 
		            $json2['status'] ="failed";
		            $json2['message'] = constant('invalid_request_'.$language);
		            echoResponse(200,$json2);
		        }
		    }
		    else
		    {
		        $check['status'] = "failed";
		        $check['message'] = constant('admin_status_'.$language);
		        unset($query_login['data']);
		        echoResponse(200,$check);
		    }
        }else
        {
          $json1['status'] ="failed";
          $json1['message'] ="No Request parameter";
          echoResponse(200,$json1);
        } 
   }
   else
   {
     $check['status'] = "false";
     $check['message'] = "Invalid Token";
     unset($check['data']);
     echoResponse(200,$check);
   }
  }else
  {
    $check['status'] = "false";
    $check['message'] = "Unauthorised access";
    unset($check['data']);
    echoResponse(200,$check);
  } 
});


$app->post('/edit_post',function() use ($app){
  $headers = get_header_token();
  if(!empty($headers['secret_key']))
  {
    $check = token_auth($headers['secret_key']);
    if($check['status']=="true")
    {
        $json1 = file_get_contents('php://input');
        if(!empty($json1))
        {
        	$data = json_decode($json1);
	        $language = $data->language;
	      	if($check['data'][0]['admin_status'] == 0)
	      	{	 
	        	// print_r($data);

	         	// ( [car_name] => woooo [model] => 1 [version] => 456 [year] => 1596 [mileage] => 12 [price] => 250 [colour] => red [car_type] => 1 [	car_condition] => good [image] => ssdsdsdsdsd ) 
		         $post_id = $data->add_id;
		         $car_name = $data->car_name;
		         $car_name_id = $data->car_name_id;
		         $model = $data->model;
		         $model_id = $data->model_id;
		         $version = $data->version;
		         $version_id = $data->version_id;
		         $year = $data->year;
		         $mileage = $data->mileage; 
		         $price = $data->price;
		         $colour = $data->colour;
		         $car_type = $data->car_type;
		         $car_condition = $data->car_condition;
		         $image = $data->image; 
		         
		         $user_id =  $check['data'][0]['user_id'];
         
		         if(!empty($car_name) && !empty($car_name_id) && !empty($model) && !empty($model_id) && !empty($version) && !empty($version_id) && !empty($year) && !empty($mileage) && !empty($price) && !empty($colour) && !empty($car_type) && !empty($car_condition) )
		         { 
          
          			$data = array(
                       'car_name'=>$car_name,
                       'car_name_id'=>$car_name_id,
                       'model' => $model,
                       'model_id' => $model_id,
                       'version'=>$version,
                       'version_id'=>$version_id,
                       'color' => $colour,
                       'car_condition' => $car_condition,
                       'car_type'=>$car_type,
                       'year' => $year,
                       'mileage'=>$mileage,
                       'price'=>$price, 
                       'user_id'=>$user_id,
                       //'status'=>1,
                       'update_at'=>dateTime
                       );

	              	global $db;  
	              	$selpost = $db->select("add_post","post_id",array('add_id'=>$post_id));
	              	if($selpost['status']=="success")
	              	{
		              	$rowss = $db->update("add_post",$data,array('add_id'=>$post_id),array());
		              	if($rowss['status']=='success')
		              	{
	                 		
	                   		$img_arr = array();
	                   		if (!empty($image )) { 

	                        $rows = $db->delete("car_image",array('mix_id'=>$post_id,'type'=>1 ));

	                        	for($i=0; $i<count($image); $i++)
	                        	{ 
		                          //print_r($image[0]);

		                          // $image1 = md5(militime.$image[$i]).'.png';
		                          // //print_r($image1);
		                          // move_uploaded_file($_FILES["image"]["tmp_name"][$i],"../../uploads/all_post/".$image1);
		                          $image_name = 'img'.$i.'_'.militime.$check['data'][0]['user_id'].'.jpeg';
		                          //exit;
		                          $path = path.'all_post/'.$image_name;
		                        
		                          $base64img = str_replace('data:image/jpeg;base64,', '', $image[$i]);
		                        
		                          $data1 = base64_decode($base64img);
		                        
		                          $aa = file_put_contents($path, $data1);

		                          $ins_img = $db->insert('car_image',array('mix_id'=>$post_id,'user_id'=>$user_id,'image'=>$image_name,'type'=>1),array());
		                          //$img_arr[] = base_url.'uploads/all_post/'.$image_name;
		                        }
	                  		}
				                  $seleimage = $db->select("car_image","image",array('mix_id'=>$post_id,'type'=>1));
				                  if($seleimage['status']=="success")
				                  {
				                     foreach ($seleimage['data'] as $value) {
				                          $img_arr[] = base_url.'uploads/all_post/'.$value['image'];
				                      } 
				                  }
			                	$workstatusdelete = 1;
			                  	$garagestatus = $db->select("add_post","delete_status",array('add_id'=>$post_id));
			            		if($garagestatus['status']=="success")
			            		{
			            			$workstatusdelete = $garagestatus['data'][0]['delete_status'];
			            		}

	            			$data['add_id'] = $post_id;
		            		$data['delete_status'] = $workstatusdelete; 	
		                  	$data['image'] = $img_arr;
		                  	$rowss["status"] = "success"; 
		                  	$rowss["message"] = constant('post_edit_'.$language.'_1');
		                  	$rowss['data'] = $data;
	                  		echoResponse(200, $rowss);
		              	}
		             	else
		              	{
		                  $rowss["status"] = "failed"; 
		                  $rowss["message"] = constant('some_error_'.$language);
		                  unset($rowss['data']);
		                  echoResponse(200, $rowss);
		              	} 
	              	}else
	              	{
	              		$rowss["status"] = "failed"; 
		                $rowss["message"] = constant('post_add_'.$language.'_2');
		                unset($rowss['data']);
		                echoResponse(200, $rowss);
	              	}
		        }
		        else
		        { 
		            $json2['status'] ="failed";
		            $json2['message'] = constant('invalid_request_'.$language);
		            echoResponse(200,$json2);
		        }
	      	}
	      	else
	      	{
	        	$check['status'] = "failed";
	        	$check['message'] = constant('admin_status_'.$language);
	        	unset($query_login['data']);
	        	echoResponse(200,$check);
	      	}
        }else
        {
          $json1['status'] ="failed";
          $json1['message'] ="No Request parameter";
          echoResponse(200,$json1);
        } 
   }
   else
   {
     $check['status'] = "false";
     $check['message'] = "Invalid Token";
     unset($check['data']);
     echoResponse(200,$check);
   }
  }else
  {
    $check['status'] = "false";
    $check['message'] = "Unauthorised access";
    unset($check['data']);
    echoResponse(200,$check);
  } 
});

$app->post('/delete_post',function() use ($app){
  $headers = get_header_token();
  if(!empty($headers['secret_key']))
  {
    $check = token_auth($headers['secret_key']);
    if($check['status']=="true")
    {
    	$json1 = file_get_contents('php://input');
     	$data = json_decode($json1);
     	$language = $data->language;
      	if($check['data'][0]['admin_status'] == 0)
      	{ 
         	$add_id = $data->add_id;
         	$delete_status = $data->delete_status;
         	$user_id = $check['data'][0]['user_id'];
	        if (!empty($add_id))
	        {
           		global $db;
           		$selpost = $db->select("add_post","post_id",array('add_id'=>$post_id));
              	if($selpost['status']=="success")
              	{
	           		$rows = $db->update("add_post",array('delete_status'=>$delete_status),array('add_id'=>$add_id,'user_id'=>$check['data'][0]['user_id']),array());
		           	if($rows['status'] == 'success')
		           	{
		             	$data = $img_arr =array();
		             	$selectwork = $db->select("add_post","*",array('add_id'=>$add_id));
		             	if($selectwork['status']=='success')
		             	{
		             		$seleimage = $db->select("car_image","image",array('mix_id'=>$add_id,'type'=>1));
		                  	if($seleimage['status']=="success")
		                  	{
			                    foreach ($seleimage['data'] as $value) {
			                        $img_arr[] = base_url.'uploads/all_post/'.$value['image'];
			                    } 
		                  	}
		             		$data = array(
		                       'car_name'=>$selectwork['data'][0]['car_name'],
		                       'model' => $selectwork['data'][0]['model'],
		                       'color' => $selectwork['data'][0]['color'],
		                       'car_condition' => $selectwork['data'][0]['car_condition'],
		                       'car_type'=>$selectwork['data'][0]['car_type'],
		                       'version'=>$selectwork['data'][0]['version'],
		                       'year' => $selectwork['data'][0]['year'],
		                       'mileage'=>$selectwork['data'][0]['mileage'],
		                       'price'=>$selectwork['data'][0]['price'],
		                       'user_id'=>$user_id,
		                       'car_name_id'=>$selectwork['data'][0]['car_name_id'],
		                       'model_id'=>$selectwork['data'][0]['model_id'],
		                       'version_id'=>$selectwork['data'][0]['version_id'],
		                       'delete_status'=>$selectwork['data'][0]['delete_status'],
		                       'image'=>$img_arr
		                       );
		             	}
	             		$rows['status'] = "success";
	             		$rows['message'] = constant('post_del_'.$language.'_1');
	             		$rows['data'] = $data;
	             		echoResponse(200,$rows);
	           		}else{
	             		$rows['status'] = "failed";
	             		$rows['message'] = constant('some_error_'.$language);
	             		unset($rows['data']);
	             		echoResponse(200,$rows);
	          		}
	          	}else
	          	{
	          		$rowss["status"] = "failed"; 
	                $rowss["message"] = constant('post_add_'.$language.'_2');
	                unset($rowss['data']);
	                echoResponse(200, $rowss);
	          	}
        	}else{
	          	$json2['status'] ="failed";
	          	$json2['message'] = constant('invalid_request_'.$language);
	          	echoResponse(200,$json2);
        	}            
	    }
	    else
	    {
	        $check['status'] = "failed";
	        $check['message'] = constant('admin_status_'.$language);
	        unset($query_login['data']);
	        echoResponse(200,$check);
	    }
    }
   else
   {
     $check['status'] = "false";
     $check['message'] = "Invalid Token";
     unset($check['data']);
     echoResponse(200,$check);
   }
  }else
  {
    $check['status'] = "false";
    $check['message'] = "Unauthorised access";
    unset($check['data']);
    echoResponse(200,$check);
  } 
});


// $app->post('/add_rent',function() use ($app){
//   $headers = get_header_token();
//   if(!empty($headers['secret_key']))
//   {
//     $check = token_auth($headers['secret_key']);
//     if($check['status']=="true")
//     {
//       if($check['data'][0]['admin_status'] == 0)
//       {  
//         $car_name = $app->request()->params('car_name');
//         $model = $app->request()->params('model');
//          $version = $app->request()->params('version');
//          $year = $app->request()->params('year');
//          $mileage = $app->request()->params('mileage'); 
//          $price = $app->request()->params('price');
//          $colour = $app->request()->params('colour');
//          $car_type = $app->request()->params('car_type');
//          $car_condition = $app->request()->params('car_condition'); 

//          $user_id =  $check['data'][0]['user_id'];
         
//          if(!empty($car_name) && !empty($model) && !empty($version) && !empty($year) && !empty($mileage) && !empty($price) && !empty($colour) && !empty($car_type) && !empty($car_condition))
//          { 
//           $data = array(
//                        'car_name'=>$car_name,
//                        'model' => $model,
//                        'color' => $colour,
//                        'car_condition' => $car_condition,
//                        'car_type'=>$car_type,
//                        'version'=>$version,
//                        'year' => $year,
//                        'mileage'=>$mileage,
//                        'price'=>$price, 
//                        'user_id'=>$user_id,
//                        'status'=>1,
//                        'create_at'=>militime

//                        );

//               global $db;  
//               $rowss = $db->insert("car_rent",$data,array());
              

//               if($rowss['status']=='success')
//               {
//                  $image = $_FILES["image"]["name"];

//                   for($i=0; $i<count($image); $i++)
//                   {
//                     $image1 = md5(militime.$image[$i]).'.png';
//                     //print_r($image1);
//                     move_uploaded_file($_FILES["image"]["tmp_name"][$i],"../../uploads/all_post/".$image1);

//                     $ins_img = $db->insert('car_image',array('mix_id'=>$rowss['data'],'user_id'=>$user_id,'image'=>$image1,'type'=>2),array());
//                     $img_arr[] = base_url.'uploads/all_post/'.$image1;
//                   }

//                   $rowss["status"] = "success"; 
//                   $rowss["message"] = "Post added Successfully";
//                   echoResponse(200, $rowss);
//               }

//               else
//               {
//                   $rowss["status"] = "failed"; 
//                   $rowss["message"] = "Something went wrong";
//                   unset($rowss['data']);
//                   echoResponse(200, $rowss);
//               } 
//          }
//          else
//          { 
//             $json2['status'] ="failed";
//             $json2['message'] ="Request parameter not valid";
//             echoResponse(200,$json2);
//          }
//       }
//       else
//       {
//         $check['status'] = "failed";
//         $check['message'] = "Your spectre has been temporarily suspended as a security precaution.";
//         unset($query_login['data']);
//         echoResponse(200,$check);
//       }
//    }
//    else
//    {
//      $check['status'] = "false";
//      $check['message'] = "Invalid Token";
//      unset($check['data']);
//      echoResponse(200,$check);
//    }
//   }else
//   {
//     $check['status'] = "false";
//     $check['message'] = "Unauthorised access";
//     unset($check['data']);
//     echoResponse(200,$check);
//   } 
// });

$app->post('/add_rent',function() use ($app){
  $headers = get_header_token();
  if(!empty($headers['secret_key']))
  {
    $check = token_auth($headers['secret_key']);
    if($check['status']=="true")
    {
	    $json1 = file_get_contents('php://input');
        if(!empty($json1))
        {
	    	$data = json_decode($json1);
        	$language = $data->language;
      		if($check['data'][0]['admin_status'] == 0)
      		{	 
	         
		         $car_name = $data->car_name;
		         $car_name_id = $data->car_name_id;
		         $model = $data->model;
		         $model_id = $data->model_id;
		         $version = $data->version;
		         $version_id = $data->version_id;
		         $year = $data->year;
		         $year_from = $data->year_from;
		         $year_to = $data->year_to;
		         $text = $data->description;
		         $mileage = $data->mileage; 
		         $price = $data->price;
		         $colour = $data->colour;
		         $car_type = $data->car_type;
		         $car_condition = $data->car_condition;
		         $image = $data->image; 

		         $user_id =  $check['data'][0]['user_id'];
	         
	         	if(!empty($car_name) && !empty($car_name_id) && !empty($model) && !empty($model_id) && !empty($version) && !empty($version_id) && !empty($year_from) && !empty($year_to) && !empty($price))
	         	{ 
	         	
	          		$data = array(
	                       'car_name'=>$car_name,
	                       'car_name_id'=>$car_name_id,
	                       'model' => $model,
	                       'model_id'=>$model_id,
	                       'color' => $colour,
	                       'car_condition' => $car_condition,
	                       'car_type'=>$car_type,
	                       'version'=>$version,
	                       'version_id'=>$version_id,
	                       'year_from' => $year_from,
	                       'year_to' => $year_to,
	                       'year'=>$year,
	                       'text'=>$text,
	                       'mileage'=>$mileage,
	                       'price'=>$price, 
	                       'user_id'=>$user_id,
	                       'status'=>1,
	                       'create_at'=>militime
	                       );
	          

	              	global $db;  
	              	$rowss = $db->insert("car_rent",$data,array());
	              

	              	if($rowss['status']=='success')
	              	{
	                 //$image = $_FILES["image"]["name"];

	                  	for($i=0; $i<count($image); $i++)
	                  	{ 
		                    //print_r($image[0]);

		                    // $image1 = md5(militime.$image[$i]).'.png';
		                    // //print_r($image1);
		                    // move_uploaded_file($_FILES["image"]["tmp_name"][$i],"../../uploads/all_post/".$image1);
		                    $image_name = 'img'.$i.'_'.militime.$check['data'][0]['user_id'].'.jpeg';
		                    //exit;
		                    $path = path.'all_post/'.$image_name;
		                  
		                    $base64img = str_replace('data:image/jpeg;base64,', '', $image[$i]);
		                  
		                    $data = base64_decode($base64img);
		                  
		                    $aa = file_put_contents($path, $data);

		                    $ins_img = $db->insert('car_image',array('mix_id'=>$rowss['data'],'user_id'=>$user_id,'image'=>$image_name,'type'=>2),array());
		                    $img_arr[] = base_url.'uploads/all_post/'.$image_name;
	                  	}

	                  	$rowss["status"] = "success"; 
	                  	$rowss["message"] = constant('rent_add_'.$language.'_1');
	                 	echoResponse(200, $rowss);
	              	}
		            else
	              	{
	                  $rowss["status"] = "failed"; 
	                  $rowss["message"] = constant('some_error_'.$language);
	                  unset($rowss['data']);
	                  echoResponse(200, $rowss);
	              	} 
	         	}
	         	else
	         	{ 
	            	$json2['status'] ="failed";
	            	$json2['message'] = constant('invalid_request_'.$language);
	            	echoResponse(200,$json2);
	         	}
		    }
		    else
		    {
		        $check['status'] = "1000";
		        $check['message'] = constant('admin_status_'.$language);
		        unset($query_login['data']);
		        echoResponse(200,$check);
		    }
        }else
        {
          $json1['status'] ="failed";
          $json1['message'] = "No request parameter.";
          echoResponse(200,$json1);
        } 
   }
   else
   {
     $check['status'] = "false";
     $check['message'] = "Invalid Token";
     unset($check['data']);
     echoResponse(200,$check);
   }
  }else
  {
    $check['status'] = "false";
    $check['message'] = "Unauthorised access";
    unset($check['data']);
    echoResponse(200,$check);
  } 
});

$app->post('/edit_rent',function() use ($app){
  $headers = get_header_token();
  if(!empty($headers['secret_key']))
  {
    $check = token_auth($headers['secret_key']);
    if($check['status']=="true")
    {
    	$json1 = file_get_contents('php://input');
    	if(!empty($json1))
    	{
		    $data = json_decode($json1);
		    $language = $data->language;
	      	if($check['data'][0]['admin_status'] == 0)
	      	{ 
		         $rent_id = $data->add_id;
		         $car_name = $data->car_name;
		         $car_name_id = $data->car_name_id;
		         $model = $data->model;
		         $model_id = $data->model_id;
		         $version = $data->version;
		         $version_id = $data->version_id;
		         $year = $data->year;
		         $year_from = $data->year_from;
		         $year_to = $data->year_to;
		         $text = $data->description;
		         $mileage = $data->mileage; 
		         $price = $data->price;
		         $colour = $data->colour;
		         $car_type = $data->car_type;
		         $car_condition = $data->car_condition;
		         $image = $data->image; 

	         	$user_id =  $check['data'][0]['user_id'];

		        if(!empty($car_name) && !empty($car_name_id) && !empty($model)&& !empty($model_id) && !empty($version) && !empty($version_id) && !empty($year_from) && !empty($year_to) && !empty($price) && !empty($rent_id))
		        { 
          
          			$data = array(
                       'car_name'=>$car_name,
                       'car_name_id'=>$car_name_id,
                       'model' => $model,
                       'model_id'=>$model_id,
                       'color' => $colour,
                       'car_condition' => $car_condition,
                       'car_type'=>$car_type,
                       'version'=>$version,
                       'version_id'=>$version_id,
                       'year_from' => $year_from,
                       'year_to' => $year_to,
                       'year'=>$year,
                       'text'=>$text,
                       'mileage'=>$mileage,
                       'price'=>$price, 
                       'user_id'=>$user_id,
                       //'status'=>1,
                       'update_at'=>dateTime
                       );

		              global $db;   
		            $selerent = $db->select("car_rent",'rent_id',array('rent_id'=>$rent_id),array());
	              	if($selerent['status']=='success')
	              	{
		            	$rowss = $db->update("car_rent",$data,array('rent_id'=>$rent_id),array());
		              	if($rowss['status']=='success')
		              	{
		                 	//$image = $_FILES["image"]["name"];
		                  	$img_arr =array();  
		                  	if (!empty($image )) 
		                  	{

			                  	$rows = $db->delete("car_image",array('mix_id'=>$rent_id,'type'=>2 ));
			                  	for($i=0; $i<count($image); $i++)
			                  	{ 
				                    $image_name = 'img'.$i.'_'.militime.$check['data'][0]['user_id'].'.jpeg';
				                    //exit;
				                    $path = path.'all_post/'.$image_name;
				                  
				                    $base64img = str_replace('data:image/jpeg;base64,', '', $image[$i]);
				                  
				                    $data1 = base64_decode($base64img);
				                  
				                    $aa = file_put_contents($path, $data1);

				                    $ins_img = $db->insert('car_image',array('mix_id'=>$rent_id,'user_id'=>$user_id,'image'=>$image_name,'type'=>2),array());
			                    	//$img_arr[] = base_url.'uploads/all_post/'.$image_name;
		                  		}
		                	}
		                  	$seleimage = $db->select("car_image","image",array('mix_id'=>$rent_id,'type'=>2));
		                  	if($seleimage['status']=="success")
		                  	{
			                     foreach ($seleimage['data'] as $value) {
			                          $img_arr[] = base_url.'uploads/all_post/'.$value['image'];
			                      } 
		                  	}
		                  	$workstatusdelete = 1;
		                  	$garagestatus = $db->select("car_rent","delete_status",array('rent_id'=>$rent_id));
		            		if($garagestatus['status']=="success")
		            		{
		            			$workstatusdelete = $garagestatus['data'][0]['delete_status'];
		            		}
		            		$data['add_id'] = $rent_id;
		            		$data['delete_status'] = $workstatusdelete;
		            		$data['description'] = $text;
		            		unset($data['text']);
		                  	$data['image'] = $img_arr;
		                  	$rowss["status"] = "success"; 
		                  	$rowss["message"] = constant('rent_add_'.$language.'_3');
		                  	$rowss['data'] = $data;
		                  	echoResponse(200, $rowss);
		              	}
		              	else
		              	{
		                  $rowss["status"] = "failed"; 
		                  $rowss["message"] = constant('some_error_'.$language);
		                  unset($rowss['data']);
		                  echoResponse(200, $rowss);
		              	} 
	         		}
	              	else
	              	{
	                  $rowss["status"] = "failed"; 
	                  $rowss["message"] = constant('rent_add_'.$language.'_2');
	                  unset($rowss['data']);
	                  echoResponse(200, $rowss);
	              	}
	         	}
	         	else
	         	{ 
		            $json2['status'] ="failed";
		            $json2['message'] = constant('invalid_request_'.$language);
		            echoResponse(200,$json2);
	         	}
	      	}
	      	else
	      	{
	        	$check['status'] = "1000";
	        	$check['message'] = constant('admin_status_'.$language);
	        	unset($query_login['data']);
	        	echoResponse(200,$check);
	      	}
        }else
        {
          $json1['status'] ="failed";
          $json1['message'] ="No Request parameter";
          echoResponse(200,$json1);
        } 
   }
   else
   {
     $check['status'] = "false";
     $check['message'] = "Invalid Token";
     unset($check['data']);
     echoResponse(200,$check);
   }
  }else
  {
    $check['status'] = "false";
    $check['message'] = "Unauthorised access";
    unset($check['data']);
    echoResponse(200,$check);
  } 
});


$app->post('/delete_rent_car',function() use ($app){
$headers = get_header_token();
if(!empty($headers['secret_key']))
{
    $check = token_auth($headers['secret_key']);
    if($check['status']=="true")
    {
        $json1 = file_get_contents('php://input');
        $data = json_decode($json1);
        $language = $data->language;
      	if($check['data'][0]['admin_status'] == 0)
      	{ 
         	$post_id = $data->add_id;
         	$delete_status = $data->delete_status;
        	$user_id = $check['data'][0]['user_id'];
         	if (!empty($post_id))
         	{
	           	$selerent = $db->select("car_rent",'rent_id',array('rent_id'=>$post_id),array());
              	if($selerent['status']=='success')
              	{
	           		global $db;
		           	$rows = $db->update("car_rent",array('delete_status'=>$delete_status),array('rent_id'=>$post_id,'user_id'=>$check['data'][0]['user_id']),array());
		           	if($rows['status'] == 'success')
		           	{
		             	$data = $img_arr =array();
		             	$selectwork = $db->select("car_rent","*",array('rent_id'=>$post_id));
		             	if($selectwork['status']=='success')
		             	{
		             		$seleimage = $db->select("car_image","image",array('mix_id'=>$post_id,'type'=>2));
		                  	if($seleimage['status']=="success")
		                  	{
			                    foreach ($seleimage['data'] as $value) {
			                        $img_arr[] = base_url.'uploads/all_post/'.$value['image'];
			                    } 
		                  	}
		             		$data = array(
		                       'car_name'=>$selectwork['data'][0]['car_name'],
		                       'car_name_id'=>$selectwork['data'][0]['car_name_id'],
		                       'model_id'=>$selectwork['data'][0]['model_id'],
		                       'model' => $selectwork['data'][0]['model'],
		                       'color' => $selectwork['data'][0]['color'],
		                       'car_condition' => $selectwork['data'][0]['car_condition'],
		                       'car_type'=>$selectwork['data'][0]['car_type'],
		                       'version'=>$selectwork['data'][0]['version'],
		                       'version_id'=>$selectwork['data'][0]['version_id'],
		                       'year_from' => $selectwork['data'][0]['year_from'],
		                       'year_to' => $selectwork['data'][0]['year_to'],
		                       'description'=>$selectwork['data'][0]['text'],
		                       'year' => $selectwork['data'][0]['year'],
		                       'mileage'=>$selectwork['data'][0]['mileage'],
		                       'price'=>$selectwork['data'][0]['price'],
		                       'user_id'=>$user_id,
		                       'delete_status'=>$selectwork['data'][0]['delete_status'],
		                       'image'=>$img_arr
		                    );
	            		}
		             	$rows['status'] = "success";
		             	$rows['message'] = constant('rent_add_'.$language.'_4');
		             	$rows['data'] = $data;
		             	echoResponse(200,$rows);
		           	}else{
			            $rows['status'] = "failed";
			            $rows['message'] = constant('some_error_'.$language);
			            unset($rows['data']);
			            echoResponse(200,$rows);
		           	}
		        }else
		        {
		        	$rows['status'] = "failed";
		            $rows['message'] = constant('rent_add_'.$language.'_2');
		            unset($rows['data']);
		            echoResponse(200,$rows);	
		        }
	        }else{
	          $json2['status'] ="failed";
	          $json2['message'] = constant('invalid_request_'.$language);
	          echoResponse(200,$json2);
	        }            
      	}
      	else
      	{
	        $check['status'] = "1000";
	        $check['message'] = constant('admin_status_'.$language);
	        unset($query_login['data']);
	        echoResponse(200,$check);
      	}
    }
   else
   {
     $check['status'] = "false";
     $check['message'] = "Invalid Token";
     unset($check['data']);
     echoResponse(200,$check);
   }
  }else
  {
    $check['status'] = "false";
    $check['message'] = "Unauthorised access";
    unset($check['data']);
    echoResponse(200,$check);
  } 
});

$app->post('/add_post_list', function() use ($app){
$headers = get_header_token();
if(!empty($headers['secret_key']))
{
  	$check = token_auth($headers['secret_key']);
  	if($check['status']=="true")
    {
     	$json1 = file_get_contents('php://input');
     	if(!empty($json1))
     	{
       		$data = json_decode($json1);
           	$language = $data->language;
	      	if($check['data'][0]['admin_status'] == 0)
	      	{  
           		$start_date = $data->create_at;
           		$list_type = $data->list_type;

             	global $db;
             	$user_id =  $check['data'][0]['user_id']; 
              
             	$start ='';
             	if($start_date != 0 && $start_date != '')
             	{
	              	if($list_type == 0){
	                 	$start = "AND (create_at < '$start_date')";
	              	}else
          		 	{
                  		$start = "AND (create_at > '$start_date')";
              		}
             	}
	            $select_post = $db->customQueryselect("SELECT * FROM add_post WHERE user_id = ".$user_id." ".$start." ORDER BY add_id DESC LIMIT 10");
	            if($select_post['status'] == 'success')
	            {
	               	foreach ($select_post['data'] as $key)
	               	{
		                $arry2 = array();
		               	$car_image = $db->customQueryselect("SELECT * FROM car_image WHERE mix_id = '".$key['add_id']."' AND type = 1 ");
		                if($car_image['status']=="success")
		                {
			                foreach($car_image['data'] as $key5)
			                {
			                     $arry2[] = base_url."uploads/all_post/".$key5['image'];
			                }
		                }
		                else
		                {
		                    $arry2 =array();
		                }
	                  		//print_r($car_image);

	                  	$arr[] = array(
                  	            'add_id' =>$key['add_id'],
                                'user_id' =>$key['user_id'],
                                'car_name'=>$key['car_name'],
                                'car_name_id'=>$key['car_name_id'],
                                'model' => $key['model'],
                                'model_id' => $key['model_id'],
                                'color' => $key['color'],
                                'car_condition' => $key['car_condition'],
                                'car_type'=>$key['car_type'],
                                'version'=>$key['version'],
                                'version_id'=>$key['version_id'],
                                'year' => $key['year'],
                                'mileage'=>$key['mileage'],
                                'price'=>$key['price'], 
                                'user_id'=>$key['user_id'],
                                'delete_status'=>$key['delete_status'],
                                'create_at'=>$key['create_at'],
                                'image'=>$arry2,
                               );
	                }
	                $select_post['status'] = "success";
	                $select_post['message'] = constant('post_add_'.$language.'_3');
	                $select_post['data'] = $arr;
	                echoResponse(200,$select_post);
	            }else{
	              $select_post['status'] = "failed";
	              $select_post['message'] = constant('post_add_'.$language.'_2');
	              unset($select_post['data']);
	              echoResponse(200,$select_post);
	            }
	      	}else
	      	{
		        $check['status'] = "1000";
		        $check['message'] = constant('admin_status_'.$language);
		        unset($check['data']);
		        echoResponse(200,$check);
	      	}
        }
        else
        { 
          $check['status'] = "failed";
          $check['message'] ="No Request parameter";
          unset($check['data']);
          echoResponse(200,$check);
        }         
    }else
    {
      $check['status'] = "false";
      $check['message'] = "Invalid Token";
      unset($check['data']);
      echoResponse(200,$check);
    }
  }else
  {
    $check['status'] = "false";
     $check['message'] = "Unauthorised access";
    unset($check['data']);
    echoResponse(200,$check);
  } 
});

$app->post('/all_add_post_list', function() use ($app){
  $headers = get_header_token();
  if(!empty($headers['secret_key']))
  {
  	$check = token_auth($headers['secret_key']);

  	if($check['status']=="true")
    {
        $json1 = file_get_contents('php://input');
        if(!empty($json1))
        {
           	$data = json_decode($json1);
	        $language = $data->language;
	      	if($check['data'][0]['admin_status'] == 0)
	      	{  
	           	$start_date = $data->create_at;
	          	$list_type = $data->list_type;
	            global $db;
	            $user_id =  $check['data'][0]['user_id']; 
	            $start ='';
	            if($start_date != 0 && $start_date != '')
	            {
	              	if($list_type == 0){
	                 	$start = "AND (create_at < '$start_date')";
	              	}else
	              	{
	                  	$start = "AND (create_at > '$start_date')";
	              	}
	            }
            
	            $select_post = $db->customQueryselect("SELECT * FROM add_post WHERE ".$start." ORDER BY add_id DESC LIMIT 10");
	            if($select_post['status'] == 'success')
	            {
	                foreach ($select_post['data'] as $key)
	                {
	                  $arry2 = array();
	               	  $car_image = $db->customQueryselect("SELECT * FROM car_image WHERE mix_id = '".$key['add_id']."' ");
	                  if($car_image['status']=="success")
	                  {
		                  foreach($car_image['data'] as $key5)
		                  {
		                     $arry2[] = base_url."uploads/all_post/".$key5['image'];
		                  }
	                  }
	                  else
	                  {
	                     $arry2 =array();
	                  }
	                 
	                  $arr[] = array(
	                  	            'add_id' =>$key['add_id'],
	                                'user_id' =>$key['user_id'],
	                                'car_name'=>$key['car_name'],
	                                'car_name_id'=>$key['car_name_id'],
	                                'model' => $key['model'],
	                                'model_id' => $key['model_id'],
	                                'color' => $key['color'],
	                                'car_condition' => $key['car_condition'],
	                                'car_type'=>$key['car_type'],
	                                'version'=>$key['version'],
	                                'version_id'=>$key['version_id'],
	                                'year' => $key['year'],
	                                'mileage'=>$key['mileage'],
	                                'price'=>$key['price'], 
	                                'user_id'=>$key['user_id'],
	                                'create_at'=>$key['create_at'],
	                                'image'=>$arry2,
	                               );
	                }
	                $select_post['status'] = "success";
	                $select_post['message'] = constant('post_add_'.$language.'_3');
	                $select_post['data'] = $arr;
	                echoResponse(200,$select_post);
	            }else{
	              $select_post['status'] = "failed";
	              $select_post['message'] = constant('post_add_'.$language.'_2');
	              unset($select_post['data']);
	              echoResponse(200,$select_post);
	            }
		    }else
		    {
		        $check['status'] = "1000";
		        $check['message'] = constant('admin_status_'.$language);
		        unset($check['data']);
		        echoResponse(200,$check);
		    }
        }
        else
        { 
          $check['status'] = "failed";
          $check['message'] ="No Request parameter";
          unset($check['data']);
          echoResponse(200,$check);
        }         
    }else
    {
      $check['status'] = "false";
      $check['message'] = "Invalid Token";
      unset($check['data']);
      echoResponse(200,$check);
    }
  }else
  {
    $check['status'] = "false";
    $check['message'] = "Unauthorised access";
    unset($check['data']);
    echoResponse(200,$check);
  } 
});

$app->post('/car_rent_list', function() use ($app){
$headers = get_header_token();
if(!empty($headers['secret_key']))
{
  	$check = token_auth($headers['secret_key']);
  	if($check['status']=="true")
    {
     	$json1 = file_get_contents('php://input');
     	if(!empty($json1))
     	{
       		$data = json_decode($json1);
       		$language = $data->language;
		  	if($check['data'][0]['admin_status'] == 0)
		  	{  
            	$start_date = $data->create_at;
           		$list_type = $data->list_type;

             	global $db;
             	$user_id =  $check['data'][0]['user_id']; 
              
	            $start ='';
	            if($start_date != 0 && $start_date != '')
	            {
	              	if($list_type == 0){
	                 	$start = "AND (create_at < '$start_date')";
	              	}else
	              	{
	                  	$start = "AND (create_at > '$start_date')";
	              	}
	            }
            
            	$select_post = $db->customQueryselect("SELECT * FROM car_rent WHERE user_id = ".$user_id." ".$start." ORDER BY rent_id DESC LIMIT 10");

            	if($select_post['status'] == 'success')
            	{
               		foreach ($select_post['data'] as $key)
               		{
                 		$arry2 = array();
                  		$car_image = $db->customQueryselect("SELECT * FROM car_image WHERE mix_id = '".$key['rent_id']."' AND type = 2 ");
	                  	if($car_image['status']=="success")
	                  	{
	                    	foreach($car_image['data'] as $key5)
	                    	{
	                       		$arry2[] = base_url."uploads/all_post/".$key5['image'];
	                    	}
	                  	}
	                  	else
	                  	{
	                     	$arry2 =array();
	                  	}
                 
                  		$arr[] = array(
                                'add_id'=>$key['rent_id'],
                                'user_id' =>$key['user_id'],
                                'car_name'=>$key['car_name'],
                                'car_name_id'=>$key['car_name_id'],
                                'model' => $key['model'],
                                'model_id' => $key['model_id'],
                                'color' => $key['color'],
                                'car_condition' => $key['car_condition'],
                                'car_type'=>$key['car_type'],
                                'version'=>$key['version'],
                                'version_id'=>$key['version_id'],
                                'year' => $key['year'],
                                'year_from' => $key['year_from'],
                                'year_to' => $key['year_to'],
                                'description'=>$key['text'],
                                'mileage'=>$key['mileage'],
                                'price'=>$key['price'], 
                                'user_id'=>$key['user_id'],
                                'delete_status'=>$key['delete_status'],
                                'create_at'=>$key['create_at'],
                                'image'=>$arry2
                               );
                	}
	                $select_post['status'] = "success";
	                $select_post['message'] = constant('rent_add_'.$language.'_5');
	                $select_post['data'] = $arr;
	                echoResponse(200,$select_post);
                }else{
	              	$select_post['status'] = "failed";
	              	$select_post['message'] = constant('rent_add_'.$language.'_2');
	              	unset($select_post['data']);
	              	echoResponse(200,$select_post);
	            }
	      	}else
	      	{
		        $check['status'] = "1000";
		        $check['message'] = constant('admin_status_'.$language);
		        unset($check['data']);
		        echoResponse(200,$check);
	      	}
        }
        else
        { 
          $check['status'] = "failed";
          $check['message'] ="No Request parameter";
          unset($check['data']);
          echoResponse(200,$check);
        }         
    }else
    {
      $check['status'] = "false";
      $check['message'] = "Invalid Token";
      unset($check['data']);
      echoResponse(200,$check);
    }
  }else
  {
    $check['status'] = "false";
     $check['message'] = "Unauthorised access";
    unset($check['data']);
    echoResponse(200,$check);
  } 
});

$app->post('/garage_work',function() use ($app)
{
	$headers = get_header_token();
	if(!empty($headers['secret_key']))
	{
	    $check = token_auth($headers['secret_key']);
	    if($check['status']=="true")
	    {
	      	$json1 = file_get_contents('php://input');
	      	if(!empty($json1))
	      	{
	            $data = json_decode($json1);
		        $language = $data->language;
		      	if($check['data'][0]['admin_status'] == 0)
		      	{  
		            $car_name = $data->car_name;
		            $model = $data->model;
		            $version = $data->version;
		            $year = $data->year;
		            $mileage = $data->mileage; 
		            $problem = $data->problem;
		            $colour = $data->colour;
		            $car_type = $data->car_type;
		            $car_name_id = $data->car_name_id;
		            $model_id = $data->model_id;
		            $car_condition = $data->car_condition;
		            $version_id = $data->version_id; 
		            $price = $data->price; 
		            $car_modified = $data->car_modified; 
		            $image = $data->image; 
		            $user_id =  $check['data'][0]['user_id'];
	         
		            if(!empty($car_name) && !empty($model) && !empty($version) && !empty($year) && !empty($mileage) && !empty($problem) && !empty($colour) && !empty($car_type) && !empty($car_condition) && !empty($price) )
		            { 
	            		$data = array(
	                       'car_name'=>$car_name,
	                       'model' => $model,
	                       'color' => $colour,
	                       'car_condition' => $car_condition,
	                       'car_type'=>$car_type,
	                       'version'=>$version,
	                       'year' => $year,
	                       'mileage'=>$mileage,
	                       'problem'=>$problem, 
	                       'user_id'=>$user_id,
	                       'status'=>1,
	                       'car_name_id'=>$car_name_id,
	                       'model_id'=>$model_id,
	                       'version_id'=>$version_id,
	                       'price'=>$price,
	                       'car_modified'=>$car_modified,
	                       'create_at'=>militime
	                    );

	                  	global $db;  
	                  	$rowss = $db->insert("garage_work",$data,array()); 
	              		if($rowss['status']=='success')
	              		{
	                  		$img_arr = array();
	                  		if (!empty($image )) { 
	                          	for($i=0; $i<count($image); $i++)
	                          	{ 
	     
		                            $image_name = 'img'.$i.'_'.militime.$user_id.'.jpeg';
		                            //exit;
		                            $path = path.'all_post/'.$image_name;
		                          
		                            $base64img = str_replace('data:image/jpeg;base64,', '', $image[$i]);
		                          
		                            $data1 = base64_decode($base64img);
		                          
		                            $aa = file_put_contents($path, $data1);

		                            $ins_img = $db->insert('car_image',array('mix_id'=>$rowss['data'],'user_id'=>$user_id,'image'=>$image_name,'type'=>3),array());
	                            	//$img_arr[] = base_url.'uploads/all_post/'.$image_name;
	                          	}
	                  		}
		                    $rowss["status"] = "success"; 
		                    $rowss["message"] = constant('work_add_'.$language.'_1');
	                      	echoResponse(200, $rowss);
	                  	}
	                  	else
	                  	{
		                    $rowss["status"] = "failed"; 
		                    $rowss["message"] = constant('some_error_'.$language);
		                    unset($rowss['data']);
		                    echoResponse(200, $rowss);
	                  	} 
		            }
		            else
		            { 
		                $json2['status'] ="failed";
		                $json2['message'] = constant('invalid_request_'.$language);
		                echoResponse(200,$json2);
		            }
		      	}
		      	else
		      	{
			        $check['status'] = "1000";
			        $check['message'] = constant('admin_status_'.$language);
			        unset($query_login['data']);
			        echoResponse(200,$check);
		      	}
	      	}else
	      	{
	            $json1['status'] ="failed";
	            $json1['message'] ="No Request parameter";
	            echoResponse(200,$json1);
	      	} 
	   	}
	   	else
	   	{
		    $check['status'] = "false";
		    $check['message'] = "Invalid Token";
		    unset($check['data']);
		    echoResponse(200,$check);
	   	}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		unset($check['data']);
		echoResponse(200,$check);
	} 
});

$app->post('/garage_work_list', function() use ($app)
{
  	$headers = get_header_token();
  	if(!empty($headers['secret_key']))
  	{
  		$check = token_auth($headers['secret_key']);

	  	if($check['status']=="true")
	    {
	        $json1 = file_get_contents('php://input');
	        if(!empty($json1))
	        {
	           	$data = json_decode($json1);
		        $language = $data->language;
			    if($check['data'][0]['admin_status'] == 0)
			    {  
		           	$start_date = $data->create_at;
		           	$list_type = $data->list_type;

             		global $db;
             		$user_id =  $check['data'][0]['user_id']; 
              
		            $start ='';
		            if($start_date != 0 && $start_date != '')
		            {
		              	if($list_type == 0){
		                	$start = "AND (create_at < '$start_date')";
		             	}else
		              	{
		                  $start = "AND (create_at > '$start_date')";
		              	}
		            }
            
		            $select_post = $db->customQueryselect("SELECT * FROM garage_work WHERE user_id = ".$user_id." ".$start."  ORDER BY work_id DESC LIMIT 10");

		            if($select_post['status'] == 'success')
		            {
		               foreach ($select_post['data'] as $key)
		               {
		                  	$img_arr =array();
		                  	$seleimage = $db->select("car_image","image",array('mix_id'=>$key['work_id'],'type'=>3));
		                  	if($seleimage['status']=="success")
		                  	{
		                     	foreach ($seleimage['data'] as $value) {
		                          $img_arr[] = base_url.'uploads/all_post/'.$value['image'];
		                      	} 
		                  	}		

                  			$arr[] = array(
                                'add_id' =>$key['work_id'],
                                'user_id' =>$key['user_id'],
                                'car_name'=>$key['car_name'],
                                'model' => $key['model'],
                                'color' => $key['color'],
                                'car_condition' => $key['car_condition'],
                                'car_type'=>$key['car_type'],
                                'version'=>$key['version'],
                                'year' => $key['year'],
                                'mileage'=>$key['mileage'],
                                'problem'=>$key['problem'], 
                                'user_id'=>$key['user_id'],
                                'create_at'=>$key['create_at'],
                                'car_name_id'=>$key['car_name_id'],
								'model_id'=>$key['model_id'],
								'version_id'=>$key['version_id'],
								'delete_status'=>$key['delete_status'],
								'price'=>$key['price'],
								'car_modified'=>$key['car_modified'],
                                'image'=>$img_arr
                            );
		                }
		                  $select_post['status'] = "success";
		                  $select_post['message'] = constant('work_add_'.$language.'_2');
		                  $select_post['data'] = $arr;
		                  echoResponse(200,$select_post);
		            }else{
	              		$select_post['status'] = "failed";
		              	$select_post['message'] = constant('rent_add_'.$language.'_2');
		              	unset($select_post['data']);
		              	echoResponse(200,$select_post);
		            }
		      	}else
		      	{
			        $check['status'] = "1000";
			        $check['message'] = constant('admin_status_'.$language);
			        unset($check['data']);
			        echoResponse(200,$check);
		      	}
	        }
	        else
	        { 
	          $check['status'] = "failed";
	          $check['message'] ="No Request parameter";
	          unset($check['data']);
	          echoResponse(200,$check);
	        }         
    	}else
    	{
	     	$check['status'] = "false";
	      	$check['message'] = "Invalid Token";
	      	unset($check['data']);
	      	echoResponse(200,$check);
    	}
  	}else
  	{
    	$check['status'] = "false";
     	$check['message'] = "Unauthorised access";
    	unset($check['data']);
    	echoResponse(200,$check);
  	} 
});

$app->post('/change_password',function() use ($app)
{
	$headers = get_header_token();
	if(!empty($headers['secret_key']))
	{
	    $check = token_auth($headers['secret_key']);
	    if($check['status']=="true")
	    {
            $json1 = file_get_contents('php://input');
            if(!empty($json1))
            {
               	$data = json_decode($json1);
	            $language = $data->language;
		        if($check['data'][0]['admin_status'] == 0) 
		        {    
	               $current_password = $data->current_password;
	               $new_password = $data->new_password;
	               global $db;

	               	if(!empty($current_password) && !empty($new_password))
	               	{
	                   $user_id = $check['data'][0]['user_id'];
	                   $condition = array('user_id'=>$user_id);
	                   $password = $check['data'][0]['password'];
	                   $cc_password = sha1($current_password);
	                   if($cc_password == $password)
	                   {
	                      	$data =array(
	                                   'password' => sha1($new_password),
	                                   'update_at'=>militime,
	                                  );

	                      	$rows = $db->update("user",$data,$condition,array());
	                     	if($rows['status'] =='success')
	                      	{
		                        $check['status'] = 'success';
		                        $check['message'] = constant('changed_'.$language.'_1');
		                        unset($check["data"]);
		                        echoResponse(200,$check);
	                      	}
	                      	else
	                      	{
	                          	$check["status"] = "failed";
	                          	$check['message'] =  constant('some_error_'.$language);
	                          	unset($check["data"]);
	                          	echoResponse(200,$check);
	                      	}
	                   	}
	                   	else
	                   	{   
	                       $check["status"] = "failed";
	                       $check['message'] = constant('changed_'.$language.'_3');
	                       unset($check["data"]);
	                       echoResponse(200,$check); 
	                   	}                                
			        }
		           	else
		           	{
		               $json2['status'] ="failed";
		               $json2['message'] = constant('invalid_request_'.$language);
		               echoResponse(200,$json2);
		           	}
               	}
		        else
		        {
		            $check['status'] = "1000";
		            $check['message'] = constant('admin_status_'.$language);
		            unset($check['data']);
		            echoResponse(200,$check);
		        }       
            }
            else
            {   
                $json2['status'] ="failed";
                $json1['message'] ="No Request parameter";
                echoResponse(200,$json1);  
            }
	    }
	    else
	    {
          	$check['status'] = "false";
          	$check['message'] = "Invalid Token";
          	unset($check['data']);
          	echoResponse(200,$check);
	    }
	}
	else
	{
	  $check['status'] = "false";
	  $check['message'] = "Unauthorised access";
	  unset($check['data']);
	  echoResponse(200,$check);
	}
});

$app->post('/car_name_list',function() use ($app)
{
	$json1 = file_get_contents('php://input');
 	$data = json_decode($json1);
 	global $db;
 	if(isset($data->language))
 	{
	 	$language = $data->language;
	 	$select_post = $db->customQueryselect("SELECT * FROM car_name WHERE status = 1 ORDER BY car_name_id ASC ");
	 	if($select_post['status'] == 'success')
	    {
	       foreach ($select_post['data'] as $key)
	       {
	          $arr[] = array(
	                        'id' =>$key['car_name_id'],
	                        'car_name'=>$key['car_name'],
	                       );
	        }
	        $select_post['status'] = "success";
	        $select_post['message'] = "successfully";
	        $select_post['data'] = $arr;
	        echoResponse(200,$select_post);
	    }else{
	      	$select_post['status'] = "failed";
	      	$select_post['message'] = constant('carname_'.$language);
	      	unset($select_post['data']);
	      	echoResponse(200,$select_post);
	    }        
 	}else
 	{
 		$json1['status'] ="failed";
        $json1['message'] ="No Request parameter";
        echoResponse(200,$json1);
 	}
});

$app->post('/model_name_list',function() use ($app)
{
    $json1 = file_get_contents('php://input');
    if(!empty($json1))
    {
     	$data = json_decode($json1);
     	$car_name_id = $data->car_name_id;
     	$language = $data->language;
     	if(!empty($car_name_id) && !empty($language))
     	{
         	global $db;
         	$select_post = $db->customQueryselect("SELECT * FROM model_name WHERE car_name_id = $car_name_id AND status = 1 ORDER BY model_id ASC LIMIT 10");
         	if($select_post['status'] == 'success')
            {
                foreach ($select_post['data'] as $key)
                {
                  $arr[] = array(
                        	'id' =>$key['model_id'],
                        	'model_name'=>$key['model_name'],
                               );
                }
                $select_post['status'] = "success";
                $select_post['message'] = "successfully";
                $select_post['data'] = $arr;
                echoResponse(200,$select_post);
            }else{
              	$select_post['status'] = "failed";
              	$select_post['message'] = constant('carname_'.$language);
              	unset($select_post['data']);
              	echoResponse(200,$select_post);
            }
      	}else
      	{
	        $json2['status'] ="failed";
	        $json2['message'] = constant('invalid_request_'.$language);
	        echoResponse(200,$json2);
      	}
    }else
    {
		$json1['status'] ="failed";
		$json1['message'] ="No Request parameter";
		echoResponse(200,$json1);
    } 
});

$app->post('/version_name_list',function() use ($app)
{
    $json1 = file_get_contents('php://input');
    if(!empty($json1))
    {
        $data = json_decode($json1);
        $model_id = $data->model_id;
        $language = $data->language;
        if(!empty($model_id) && !empty($language))
        {
	        global $db;
	        $select_post = $db->customQueryselect("SELECT * FROM version_name WHERE model_id = $model_id AND status = 1 ORDER BY version_id ASC LIMIT 10");
	        if($select_post['status'] == 'success')
            {
               	foreach ($select_post['data'] as $key)
               	{
                   $arr[] = array(
                            'id' =>$key['version_id'],
                            'version_name'=>$key['version_name'],
                           	);
                }
                $select_post['status'] = "success";
                $select_post['message'] = "successfully";
                $select_post['data'] = $arr;
                echoResponse(200,$select_post);
            }else{
              	$select_post['status'] = "failed";
              	$select_post['message'] = constant('carname_'.$language);
              	unset($select_post['data']);
              	echoResponse(200,$select_post);
            }
	    }else
        {
            $json2['status'] ="failed";
            $json2['message'] = constant('invalid_request_'.$language);
            echoResponse(200,$json2);
        }      
    }else
    {
      $json1['status'] ="failed";
      $json1['message'] ="No Request parameter";
      echoResponse(200,$json1);
    } 
});

$app->post('/edit_garage_work',function() use ($app)
{
  	$headers = get_header_token();
  	if(!empty($headers['secret_key']))
  	{
	    $check = token_auth($headers['secret_key']);
	    if($check['status']=="true")
	    {
	        $json1 = file_get_contents('php://input');
	        if(!empty($json1))
	        {
	        	$data = json_decode($json1);
		    	$language = $data->language;
		      	if($check['data'][0]['admin_status'] == 0)
		      	{ 
		    	    $car_name = $data->car_name;
		            $model = $data->model;
		            $version = $data->version;
		            $year = $data->year;
		            $mileage = $data->mileage; 
		            $problem = $data->problem;
		            $colour = $data->colour;
		            $car_type = $data->car_type;
		            $car_name_id = $data->car_name_id;
		            $model_id = $data->model_id;
		            $car_condition = $data->car_condition;
		            $version_id = $data->version_id; 
		            $image = $data->image; 
		            $add_id = $data->add_id; 
		            $price = $data->price; 
		            $car_modified = $data->car_modified; 
		            $user_id =  $check['data'][0]['user_id'];
		         
		            if(!empty($car_name) && !empty($model) && !empty($version) && !empty($year) && !empty($mileage) && !empty($problem) && !empty($colour) && !empty($car_type) && !empty($car_condition) && !empty($price))
		            { 
		                $data = array(
	                           'car_name'=>$car_name,
	                           'model' => $model,
	                           'color' => $colour,
	                           'car_condition' => $car_condition,
	                           'car_type'=>$car_type,
	                           'version'=>$version,
	                           'year' => $year,
	                           'mileage'=>$mileage,
	                           'problem'=>$problem, 
	                           'user_id'=>$user_id,
	                           'car_name_id'=>$car_name_id,
	                           'model_id'=>$model_id,
	                           'version_id'=>$version_id,
	                           'price'=>$price,
	                           'car_modified'=>$car_modified,
	                           'update_at'=>dateTime
	                           );
		                $response = array();
		              	global $db;  
		              	$rowss = $db->update("garage_work",$data,array('work_id'=>$add_id),array());
		              	if($rowss['status']=='success')
		              	{
		                 	//$image = $_FILES["image"]["name"];
		                   	$img_arr = array();
		                   	if (!empty($image )) { 

		                        $rows = $db->delete("car_image",array('mix_id'=>$add_id,'type'=>3 ));

		                        for($i=0; $i<count($image); $i++)
		                        { 
		                          	$image_name = 'img'.$i.'_'.militime.$check['data'][0]['user_id'].'.jpeg';
		                          
		                          	$path = path.'all_post/'.$image_name;
		                        
		                          	$base64img = str_replace('data:image/jpeg;base64,', '', $image[$i]);
		                        
		                          	$data1 = base64_decode($base64img);
		                        
		                          	$aa = file_put_contents($path, $data1);

		                          	$ins_img = $db->insert('car_image',array('mix_id'=>$add_id,'user_id'=>$user_id,'image'=>$image_name,'type'=>3),array());
		                        }
		                  	}
		                  	$seleimage = $db->select("car_image","image",array('mix_id'=>$add_id,'type'=>3));
		                  	if($seleimage['status']=="success")
		                  	{
		                     foreach ($seleimage['data'] as $value) {
		                          $img_arr[] = base_url.'uploads/all_post/'.$value['image'];
		                      } 
		                  	}
		                  	$workstatusdelete = 1;
		                  	$garagestatus = $db->select("garage_work","delete_status",array('work_id'=>$add_id));
		            		if($garagestatus['status']=="success")
		            		{
		            			$workstatusdelete = $garagestatus['data'][0]['delete_status'];
		            		}
		            		$data['delete_status'] = $workstatusdelete; 	
			                $data['image'] = $img_arr;
			                $data['add_id'] = $add_id;
			                $rowss["status"] = "success"; 
			                $rowss["message"] = constant('work_add_'.$language.'_3');
			                $rowss['data'] = $data;
			                echoResponse(200, $rowss);
		              	}
		              	else
		              	{
		                  	$rowss["status"] = "failed"; 
		                  	$rowss["message"] = constant('some_error_'.$language);
		                  	unset($rowss['data']);
		                  	echoResponse(200, $rowss);
		              	} 
		         	}
		         	else
		         	{ 
		            	$json2['status'] ="failed";
		            	$json2['message'] = constant('invalid_request_'.$language);
		            	echoResponse(200,$json2);
		         	}
		      	}
		      	else
		      	{
			        $check['status'] = "1000";
			        $check['message'] = constant('admin_status_'.$language);
			        unset($query_login['data']);
			        echoResponse(200,$check);
		      	}
	        }else
	        {
	          $json1['status'] ="failed";
	          $json1['message'] ="No Request parameter";
	          echoResponse(200,$json1);
	        } 
		}
		else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			unset($check['data']);
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		unset($check['data']);
		echoResponse(200,$check);
	} 
});

$app->post('/delete_garage_work',function() use ($app)
{
	$headers = get_header_token();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status']=="true")
		{
			$json1 = file_get_contents('php://input');
			$data = json_decode($json1);
			if($check['data'][0]['admin_status'] == 0)
			{	 
				$add_id = $data->add_id;
				$delete_status = $data->delete_status; // 1=inactive, 2=delete
				$user_id= $check['data'][0]['user_id'];
				if(!empty($add_id))
				{
					global $db;
					$rows = $db->update("garage_work",array('delete_status'=>$delete_status,'update_at'=>dateTime),array('work_id'=>$add_id,'user_id'=>$check['data'][0]['user_id']),array());
					if($rows['status'] == 'success')
					{
						$data = $img_arr =array();
						$selectwork = $db->select("garage_work","*",array('work_id'=>$add_id));
						if($selectwork['status']=='success')
						{

							$seleimage = $db->select("car_image","image",array('mix_id'=>$add_id,'type'=>3));
							if($seleimage['status']=="success")
							{
								foreach ($seleimage['data'] as $value) {
								$img_arr[] = base_url.'uploads/all_post/'.$value['image'];
								} 
							}
							$data = array(
								'car_name'=>$selectwork['data'][0]['car_name'],
								'model' => $selectwork['data'][0]['model'],
								'color' => $selectwork['data'][0]['color'],
								'car_condition' => $selectwork['data'][0]['car_condition'],
								'car_type'=>$selectwork['data'][0]['car_type'],
								'version'=>$selectwork['data'][0]['version'],
								'year' => $selectwork['data'][0]['year'],
								'mileage'=>$selectwork['data'][0]['mileage'],
								'problem'=>$selectwork['data'][0]['problem'], 
								'user_id'=>$user_id,
								'status'=>$selectwork['data'][0]['status'],
								'car_name_id'=>$selectwork['data'][0]['car_name_id'],
								'model_id'=>$selectwork['data'][0]['model_id'],
								'version_id'=>$selectwork['data'][0]['version_id'],
								'delete_status'=>$selectwork['data'][0]['delete_status'],
								'price'=>$selectwork['data'][0]['price'],
								'car_modified'=>$selectwork['data'][0]['car_modified'],
								'image'=>$img_arr
							);
						}
						$rows['status'] = "success";
						$rows['message'] = constant('work_add_'.$language.'_4');
						$rows['data'] = $data;
						echoResponse(200,$rows);
					}else{
						$rows['status'] = "failed";
						$rows['message'] = constant('some_error_'.$language);
						unset($rows['data']);
						echoResponse(200,$rows);
					}
				}else{
					$json2['status'] ="failed";
					$json2['message'] = constant('invalid_request_'.$language);
					echoResponse(200,$json2);
				}            
			}
			else
			{
				$check['status'] = "1000";
				$check['message'] = constant('admin_status_'.$language);
				unset($query_login['data']);
				echoResponse(200,$check);
			}
		}
		else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			unset($check['data']);
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		unset($check['data']);
		echoResponse(200,$check);
	} 
});

$app->post('/filter',function() use ($app)
{
	$headers = get_header_token();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status']=="true" || $headers['secret_key']=='ef73781effc5774100f87fe2f437a435')
		{
			$response = $arr = array();
			$json1 = file_get_contents('php://input');
	        if(!empty($json1))
	        {
	        	$data = json_decode($json1);
		    	$language = $data->language;
	    	    $colour = $data->colour;
	            $car_type = $data->car_type;
	            $car_name_id = $data->car_name_id;
	            $model_id = $data->model_id;
	            $version_id = $data->version_id; 
	            $year_from = $data->year_from; 
	            $year_to = $data->year_to; 
	            //$car_condition = $data->car_condition; 
	            $price_min_range = $data->price_min_range;
	            $price_max_range = $data->price_max_range;
	            $type = $data->type; // 1 = post, 2 = rent
	            $create_at = $data->create_at;
	            $list_type = $data->list_type; //0 = loadmore, 1=pulltorefresh
	            $user_id ='';
	            if($check['status']=='true')
	            {
	            	$user_id =  $check['data'][0]['user_id'];
	            }
	    		global $db;
				
				$conditions = array();
				if($type == 1)
				{
					$table_name = "add_post"; $ids = "add_id";	$type = 1;
					$conditions[] = "(add_post.status = 0 AND add_post.delete_status = 0)";
				}else
				{
					$table_name = "car_rent"; $ids = "rent_id"; $type = 2;
					$conditions[] = "(car_rent.status = 0 AND car_rent.delete_status = 0 AND DATE(car_rent.year_to) >= CURDATE())";
				}
				$query1 = "SELECT $table_name.*,user.mobile_code,user.full_name,user.email,user.mobile_no,user.address,user.image FROM $table_name INNER JOIN user ON $table_name.user_id = user.user_id";
			 	  if($user_id!=''){
					   $conditions[] = "($table_name.user_id != '$user_id')";
				  }if($price_max_range != 0) {
				     $conditions[] = "($table_name.price BETWEEN '$price_min_range' AND '$price_max_range')";
			   	}if($colour != "") {
				     $conditions[] = "($table_name.color like '%$colour%')";
			   	}if($car_type != "") {
				     $conditions[] = "($table_name.car_type like '%$car_type%')";
			   	}if($car_name_id != 0) {
				     $conditions[] = "($table_name.car_name_id = '$car_name_id')";
			   	}if($model_id != 0) {
				     $conditions[] = "($table_name.model_id = '$model_id')";
			   	}if($version_id != 0) {
				     $conditions[] = "($table_name.version_id = '$version_id')";
			   	}if($year_from != '' || $year_to != '') {
				    if($type==1){
             $conditions[] = "($table_name.year BETWEEN '$year_from' AND '$year_to')";
            }else{
              if($year_to=='')
              {
                $conditions[] = "($table_name.year_from >= '$year_from')";
              }elseif($year_from=='')
              {
                $conditions[] = "($table_name.year_to <= '$year_to')";
                //$conditions[] = "($table_name.year_from BETWEEN '$year_from' AND '$year_to')";
              }else
              {
                $conditions[] = "($table_name.year_from >= '$year_from' OR $table_name.year_to <= '$year_to')";
              }
            }
			   	}if($create_at != 0)
				  {
					if($list_type ==0)
					$conditions[] = "($table_name.create_at < '$create_at')";
					else
					$conditions[] = "($table_name.create_at > '$create_at')";
				}
				$sql1 = $query1;
				if (count($conditions) > 0) {
				    $sql1 .= " WHERE " . implode(' AND ', $conditions).' ORDER BY '.$table_name.'.create_at DESC limit 10';
				}else
				{
					  $sql1 .= ' ORDER BY '.$table_name.'.create_at DESC limit 10';
				}
				$result1 = $db->customQueryselect($sql1);
				if($result1['status']=="success")
				{
					foreach ($result1['data'] as $key) {

			            $isinterest = 0;
			            $interest = $db->select("interest","interest_id",array('user_id'=>$user_id,'second_user_id'=>$key['user_id'],'type'=>$type,'interested_id'=>$key[$ids]));
			            if($interest['status']=="success")
			            {
			                $isinterest = 1;
		            	}
						$img_arr = array(); $uimage = '';
						if($key['image']!='')
						{
							$uimage = base_url.'uploads/user_image/'.$key['image'];
						}
						$seleimage = $db->select("car_image","image",array('mix_id'=>$key[$ids],'type'=>$type));
	                  	if($seleimage['status']=="success")
	                  	{
		                    foreach ($seleimage['data'] as $value) {
		                        $img_arr[] = base_url.'uploads/all_post/'.$value['image'];
		                    } 
	                  	}
	                  	if(isset($key['text']))
	                  	{
	                  		$key['description'] = $key['text'];
	                  		unset($key['text']);
	                  	}
	                  	$key['add_id'] = $key[$ids];
	                  	$key['is_interest'] = $isinterest;
	                  	$key['user_image'] = $uimage;
	                  	$key['image'] = $img_arr;
	                  	$arr[]= $key;
					}
				}
				if($arr != '' && $arr != null)
			    {
				    $response['status']='success';
				    $response['message']='successfully';
				    $response['data'] = $arr;
					echoResponse(200,$response);
				}else
				{
					$response['status']='failed';
				    $response['message']= constant('rent_add_'.$language.'_2');
				   	echoResponse(200,$response);
				}
			}else
			{
				$response['status'] ="failed";
      			$response['message'] = "No request parameter.";
      			echoResponse(200,$response);
			}
		}	
		else
		{
			$msg['status'] = "false";
      		$msg['message'] = "Invalid Token";
			echoResponse(200,$msg);
		}
	}
	else
	{
		$msg['status'] = "false";
    	$msg['message'] = "Unauthorised access";
		echoResponse(200,$msg);
	}
});

$app->post('/garage_work_list_detail', function() use ($app)
{
	$headers = get_header_token();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status']=="true" || $headers['secret_key']=='ef73781effc5774100f87fe2f437a435')
		{
		    $json1 = file_get_contents('php://input');
		    if(!empty($json1))
		    {
		       	$data = json_decode($json1);
		        
		       	$language = $data->language;
		       	$start_date = $data->create_at;
		       	$list_type = $data->list_type;
		       	$service_type = $data->service_type; //1=maint,2=modi,0=both

		        global $db;
		        $user_id ='';
		        if($check['status']=='true')
		        {
		        	$user_id =  $check['data'][0]['user_id'];
		        } 
		          
		        $start ='';
		        if($start_date != 0 && $start_date != '')
		        {
		          	if($list_type == 0){
		             	$start = "AND (create_at < '$start_date')";
		          	}else
		          	{
		              	$start = "AND (create_at > '$start_date')";
		          	}
		        }
		        $filter_service= '';
		        if($service_type !=0)
		        {
		           	$filter_service = "AND service_type = '$service_type'";
		        }

		        $select_post = $db->customQueryselect("SELECT user.user_id,user.service_type,user.avg_rating,user.full_name,user.email,user.mobile_code,user.mobile_no,user.address,user.image,user.garage_image,user.expertise,user.car_repaire,user.create_at as create_date FROM user WHERE user.type = 2 AND user.admin_status = 0 AND user.mobile_status = 1 AND user.create_at != '' ".$filter_service." ".$start." ORDER BY user.create_at DESC LIMIT 10");
		        if($select_post['status'] == 'success')
		        {
		           	foreach ($select_post['data'] as $key)
		           	{
			            $uimage = $garageimage ='';
			            if($key['image']!='')
			            {
			              	$uimage = base_url.'uploads/user_image/'.$key['image'];
			            }	
			            if($key['garage_image']!='')
			            {
			                $garageimage = base_url.'uploads/garage_image/'.$key['garage_image'];
			            } 	
		              	$revie = $workdata =array();
		              			//echo $key['user_id'];exit;
						$reviewsel = $db->customQueryselect("SELECT user.user_id,user.full_name as user_name,user.image as user_image,rating_n_review.rating,rating_n_review.reviews,DATE_FORMAT(rating_n_review.create_date, '%d-%b-%Y') as date FROM rating_n_review INNER JOIN user ON rating_n_review.user_id = user.user_id  WHERE rating_n_review.second_user_id = '".$key['user_id']."' ORDER BY rating_n_review.id DESC LIMIT 2");
		                if($reviewsel['status']=="success")
		                {
		                	  foreach ($reviewsel['data'] as $value) {
		                    if($value['user_image']!='')
		                    {
		                      $value['user_image'] = base_url.'uploads/user_image/'.$value['user_image'];
		                    }   
		                      $revie[] = $value;
		                    }
		                }
		              	$myrevi = $revdate = ""; $myrate = "0"; 
		              	$myreviewsel = $db->customQueryselect("SELECT rating,reviews,DATE_FORMAT(create_date, '%d-%b-%Y') as date FROM rating_n_review WHERE user_id = '$user_id' AND second_user_id = '".$key['user_id']."'");
		              	if($myreviewsel['status']=="success")
		              	{
		              		$myrevi = $myreviewsel['data'][0]['reviews'];
		              		$myrate = $myreviewsel['data'][0]['rating'];
		              		$revdate = $myreviewsel['data'][0]['date'];
		              	}
		              	$is_inte = 0;
		              	$myinterest = $db->customQueryselect("SELECT interest_id FROM interest WHERE user_id = '$user_id' AND second_user_id = '".$key['user_id']."' AND type = 3");
		              	if($myinterest['status']=="success")
		              	{
		              		$is_inte = 1;
		              	}

		              	$reviewsel = $db->customQueryselect("SELECT work_id,user_id,car_name,car_name_id,model,model_id,version,version_id,color,car_condition,car_type,year,mileage,problem,delete_status,price,car_modified FROM garage_work WHERE user_id = '".$key['user_id']."' AND status = 0 AND delete_status = 0 ORDER BY work_id DESC LIMIT 2");
		              	if($reviewsel['status']=="success")
		              	{
		                  	foreach ($reviewsel['data'] as $value) {
		              			
		              			$img_arr =array();
		              			$seleimage = $db->select("car_image","image",array('mix_id'=>$value['work_id'],'type'=>3));
			                	if($seleimage['status']=="success")
			                	{
			                     	foreach ($seleimage['data'] as $valuess) {
			                          	$img_arr[] = base_url.'uploads/all_post/'.$valuess['image'];
			                      	} 
			                	}
			                    $workdata[] = array(
			                        'add_id' =>$value['work_id'],
			                        'user_id' =>$value['user_id'],
			                        'car_name'=>$value['car_name'],
			                        'car_name_id'=>$value['car_name_id'],
			                        'model' => $value['model'],
			                        'model_id'=>$value['model_id'],
			                        'version'=>$value['version'],
			                        'version_id'=>$value['version_id'],
			                        'color' => $value['color'],
			                        'car_condition' => $value['car_condition'],
			                        'car_type'=>$value['car_type'],
			                        'year' => $value['year'],
			                        'mileage'=>$value['mileage'],
			                        'problem'=>$value['problem'], 
			                        'delete_status'=>$value['delete_status'],
			                        'price'=>$value['price'],
			                        'car_modified'=>$value['car_modified'],
			                        'image'=>$img_arr
			                    );
		                	}
		            	}

		              $arr[] = array(
		                    'user_id'=>$key['user_id'],
		                    'full_name'=>$key['full_name'],
		                    'email'=>$key['email'],
		                    'mobile_code'=>$key['mobile_code'],
		                    'mobile_no'=>$key['mobile_no'],
		                    'address'=>$key['address'],
		                    'expertise'=>$key['expertise'],
		                    'car_repaire'=>$key['car_repaire'],
		                    'service_type'=>$key['service_type'],
		                    'user_image'=>$uimage,
		                    'garage_image'=>$garageimage,
		                    'create_at'=>$key['create_date'],
		                    'myreviews' =>$myrevi,
		                    'myrating'=>$myrate,
		                    'revdate'=>$revdate,
		                    'is_interest'=>$is_inte,
		                    'avg_rating'=>$key['avg_rating'],
		                    'reviews'=>$revie,
		                    //'image'=>$img_arr,
		                    'work'=>$workdata
		                  );
		            }
		            $select_post['status'] = "success";
		            $select_post['message'] = "successfully";
		            $select_post['data'] = $arr;
		            echoResponse(200,$select_post);
		        }else{
		          	$select_post['status'] = "failed";
		          	$select_post['message'] = constant('rent_add_'.$language.'_2');
		          	unset($select_post['data']);
		          	echoResponse(200,$select_post);
		        }
		    }
		    else
		    { 
		      $check['status'] = "failed";
		      $check['message'] ="No Request parameter";
		      unset($check['data']);
		      echoResponse(200,$check);
		    }         
		}else
		{
		  	$check['status'] = "false";
		  	$check['message'] = "Invalid Token";
		  	unset($check['data']);
		  	echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		unset($check['data']);
		echoResponse(200,$check);
	} 
});

$app->post('/interest',function() use ($app)
{
	$headers = get_header_token();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status']=="true")
		{
			$json1 = file_get_contents('php://input');
			$data = json_decode($json1);
			$language = $data->language;
			if($check['data'][0]['admin_status'] == 0)
			{ 
				global $db;
				$user_id = $check['data'][0]['user_id'];
				$second_user_id = $data->second_user_id;
				$interested_id = $data->interested_id;
				$type = $data->type;

				if(!empty($second_user_id) && !empty($type))
				{
				 	$select_post = $db->customQueryselect("SELECT interest_id FROM interest WHERE type = '$type' AND user_id = $user_id AND interested_id = '$interested_id' AND second_user_id = '$second_user_id'");
				 	if($select_post['status'] == 'success')
				    {
				        $select_post['status'] = "failed";
				        $select_post['message'] = "already interested.";
				        unset($select_post['data']);
				        echoResponse(200,$select_post);
				    }else{
				      	$insert = $db->insert("interest",array('user_id'=>$user_id,'second_user_id'=>$second_user_id,'interested_id'=>$interested_id,'type'=>$type,'create_date'=>dateTime,'update_date'=>dateTime),array());
				      	if($insert['status']=="success")
				      	{
				          	$token = $db->select("user","device_token,device_type",array('user_id'=>$second_user_id));
				          	if($token['status']=='success')
				          	{
				            	$devicetoken = $token['data'][0]['device_token'];
				            	$devicetype = $token['data'][0]['device_type'];
				              	$msg = 'You have received interest from '.$check['data'][0]['full_name'];
				              	if($devicetype == 'android')
				              	{
				              		$message = array('title'=>'New interest','msg'=>$msg,'interested_id'=>$interested_id,'receiver_id'=>$second_user_id,'type'=>$type,'create_at'=>militime);
				          			AndroidNotification($devicetoken,$message);
				              	}
				              	else
				              	{
				             		$title = 'New interest';
				             		$message = array('msg'=>$msg,'interested_id'=>$interested_id,'receiver_id'=>$second_user_id,'type'=>$type);
									       iOSPushNotification($devicetoken, $message ,$title);	
				              	}
				              	$notification = $db->insert("notification",array('sender_id'=>$user_id,'receiver_id'=>$second_user_id,'type'=>$type,'msg'=>$msg,'id'=>$interested_id,'create_date'=>dateTime,'update_date'=>dateTime),array());
				          	}
				          	$select_post['status'] = "success";
				          	$select_post['message'] = constant('interest_'.$language);
				          	unset($select_post['data']);
				          	echoResponse(200,$select_post);
				      	}else
				      	{
				          	$select_post['status'] = "failed";
				          	$select_post['message'] = constant('some_error_'.$language);
				          	unset($select_post['data']);
				          	echoResponse(200,$select_post);
				      	}
				    }        
				}else
				{
					$select_post['status'] = "failed";
		          	$select_post['message'] = constant('invalid_request_'.$language);
		          	unset($select_post['data']);
		          	echoResponse(200,$select_post);
				}
			}
			else
			{
				$check['status'] = "1000";
				$check['message'] = constant('admin_status_'.$language);
				unset($query_login['data']);
				echoResponse(200,$check);
			}
		}
		else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			unset($check['data']);
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		unset($check['data']);
		echoResponse(200,$check);
	} 
});

$app->post('/interest_garage',function() use ($app)
{
	$headers = get_header_token();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status']=="true")
		{
			$json1 = file_get_contents('php://input');
			$data = json_decode($json1);
			$language = $data->language; 
			if($check['data'][0]['admin_status'] == 0)
			{ 
				global $db;
				$user_id = $check['data'][0]['user_id'];
				$second_user_id = $data->second_user_id; 
				$problem = $data->problem;
				if(!empty($second_user_id))
				{
					$select_post = $db->customQueryselect("SELECT interest_id FROM interest WHERE type = 3 AND user_id = $user_id AND second_user_id = '$second_user_id'");
				 	if($select_post['status'] == 'success')
				    {
				        $select_post['status'] = "failed";
				        $select_post['message'] = constant('interest_'.$language.'_1');
				        unset($select_post['data']);
				        echoResponse(200,$select_post);
				    }else{
				      	$insert = $db->insert("interest",array('user_id'=>$user_id,'problem'=>$problem,'second_user_id'=>$second_user_id,'type'=>3,'create_date'=>dateTime,'update_date'=>dateTime),array());
				        if($insert['status']=="success")
				        {
					        $token = $db->select("user","device_token,device_type",array('user_id'=>$second_user_id));
					        if($token['status']=='success')
					        {
					            $devicetoken = $token['data'][0]['device_token'];
					            $devicetype = $token['data'][0]['device_type'];
					        }
					        $msg = 'You have received interest from '.$check['data'][0]['full_name'];
					        $message = array('title'=>'New interest','msg'=>$msg,'interested_id'=>0,'receiver_id'=>$second_user_id,'type'=>3,'create_at'=>militime);
					        AndroidNotification($devicetoken,$message);
					        $notification = $db->insert("notification",array('sender_id'=>$user_id,'receiver_id'=>$second_user_id,'type'=>3,'msg'=>$msg,'id'=>0,'create_date'=>dateTime,'update_date'=>dateTime),array());
					        $select_post['status'] = "success";
					        $select_post['message'] = constant('interest_'.$language);
					        unset($select_post['data']);
					        echoResponse(200,$select_post);
				        }else
				        {
				          $select_post['status'] = "failed";
				          $select_post['message'] = constant('some_error_'.$language);
				          unset($select_post['data']);
				          echoResponse(200,$select_post);
				        }
				    }        
				}else
				{
					$check['status'] = "failed";
					$check['message'] = constant('invalid_request_'.$language);
					unset($query_login['data']);
					echoResponse(200,$check);
				}
			}
			else
			{
				$check['status'] = "1000";
				$check['message'] = constant('admin_status_'.$language);
				unset($query_login['data']);
				echoResponse(200,$check);
			}
		}
		else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			unset($check['data']);
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		unset($check['data']);
		echoResponse(200,$check);
	} 
});

$app->post('/review_n_rating',function() use ($app)
{
	$headers = get_header_token();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status']=="true")
		{
			$json1 = file_get_contents('php://input');
			$data = json_decode($json1);
			$language = $data->language;
			if($check['data'][0]['admin_status'] == 0)
			{ 
				global $db;
				$user_id = $check['data'][0]['user_id'];
				$second_user_id = $data->second_user_id;
				$review = $data->reviews;
				$rating = $data->rating;

			  	if(!empty($second_user_id) && !empty($rating))
			  	{
				  	$select_post = $db->customQueryselect("SELECT id FROM rating_n_review WHERE second_user_id = '$second_user_id' AND user_id = '$user_id'");
				  	if($select_post['status'] == 'success')
				  	{
				      $insertt = $db->update("rating_n_review",array('reviews'=>$review,'rating'=>$rating,'update_date'=>dateTime),array('id'=>$select_post['data'][0]['id']),array());
				  	}else{
				    	$insertt = $db->insert("rating_n_review",array('user_id'=>$user_id,'second_user_id'=>$second_user_id,'reviews'=>$review,'rating'=>$rating,'create_date'=>dateTime,'update_date'=>dateTime),array());
				  	}        
					if($insertt['status']=="success")
					{
						$revarr = array();
				      	$reviewsel = $db->customQueryselect("SELECT user.user_id,user.full_name as user_name,user.image as user_image,rating_n_review.rating,rating_n_review.reviews,DATE_FORMAT(rating_n_review.create_date, '%d-%b-%Y') as date FROM rating_n_review INNER JOIN user ON rating_n_review.user_id = user.user_id  WHERE rating_n_review.second_user_id = '".$second_user_id."' ORDER BY rating_n_review.id DESC LIMIT 2");
					    if($reviewsel['status']=="success")
					    {
					        foreach ($reviewsel['data'] as $key) {
					            $revarr[] = $key;
					        }
					    }
				      	$avgrat = 0;
				      	$averat = $db->customQueryselect("SELECT avg(rating) as avgrating FROM rating_n_review WHERE second_user_id = '$second_user_id'");
				      	if($averat['status']=="success")
				      	{
				      		$avgrat = $averat['data'][0]['avgrating'];
				      	}
					    $db->update("user",array('avg_rating'=>$avgrat),array('user_id'=>$second_user_id),array());
					    $select_post['status'] = "success";
					    $select_post['message'] = constant('rating_'.$language.'_1');
					    $select_post['data'] = array('myreviews'=>$review,'myrating'=>$rating,'reviews'=>$revarr);
					    echoResponse(200,$select_post);
					}else
					{
					    $select_post['status'] = "failed";
					    $select_post['message'] = constant('some_error_'.$language);
					    unset($select_post['data']);
					    echoResponse(200,$select_post);
					}
			  	}else
			  	{
			  		$check['status'] = "failed";
					$check['message'] = constant('invalid_request_'.$language);
					unset($query_login['data']);
					echoResponse(200,$check);
			  	}
			}
			else
			{
				$check['status'] = "1000";
				$check['message'] = constant('admin_status_'.$language);
				unset($query_login['data']);
				echoResponse(200,$check);
			}
		}
		else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			unset($check['data']);
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		unset($check['data']);
		echoResponse(200,$check);
	} 
});

$app->post('/review_n_rating_list',function() use ($app)
{
	$headers = get_header_token();
	if(!empty($headers['secret_key']))
	{
	    $check = token_auth($headers['secret_key']);
	    if($check['status']=="true")
	    {
        	$json1 = file_get_contents('php://input');
            $data = json_decode($json1);
		    $language = $data->language;
	      	if($check['data'][0]['admin_status'] == 0)
	      	{ 
		        global $db;
		        $user_id = $check['data'][0]['user_id'];
		        $second_user_id = $data->second_user_id;
		        $start_date = $data->create_at;
		        $list_type = $data->list_type;
	 		    $start ='';
	            if($start_date != 0 && $start_date != '')
	            {
	              	if($list_type == 0){
	                 	$start = "AND (rating_n_review.create_at < '$start_date')";
	              	}else
	              	{
	                  	$start = "AND (rating_n_review.create_at > '$start_date')";
	              	}
	            }
	            $reviewsel = $db->customQueryselect("SELECT user.full_name as user_name,user.image as user_image,rating_n_review.rating,rating_n_review.reviews,rating_n_review.create_date as create_at FROM rating_n_review INNER JOIN user ON rating_n_review.user_id = user.user_id  WHERE rating_n_review.second_user_id = '$second_user_id' AND user.admin_status=0 AND user.mobile_status = 1 ".$start." ORDER BY rating_n_review.create_date DESC LIMIT 10");
	            if($reviewsel['status'] == 'success')
	            {
	                foreach ($reviewsel['data'] as $key) {
	                	$date = date('d-M-Y',strtotime($key['create_at']));
	                	$key['date'] = $date;
	                    $uimage = '';
	                    if($key['user_image']!='')
	                    {
	                      $key['user_image'] = base_url.'uploads/user_image/'.$key['user_image'];
	                    }  
	                	$arr[] = $key;
	                }
                  	$select_post['status'] = "success";
                  	$select_post['message'] = constant('rating_'.$language.'_2');
                  	$select_post['data'] = $arr;
                  	echoResponse(200,$select_post);
	            }else
              	{
                  	$select_post['status'] = "failed";
                  	$select_post['message'] = constant('carname_'.$language);
                  	unset($select_post['data']);
                  	echoResponse(200,$select_post);
              	}
      		}
	    	else
	      	{
		        $check['status'] = "1000";
		        $check['message'] = constant('admin_status_'.$language);
		        unset($query_login['data']);
		        echoResponse(200,$check);
	      	}
    	}
   		else
   		{
		    $check['status'] = "false";
		    $check['message'] = "Invalid Token";
		    unset($check['data']);
		    echoResponse(200,$check);
	   	}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		unset($check['data']);
		echoResponse(200,$check);
	} 
});

$app->post('/notification_list',function() use ($app)
{
	$headers = get_header_token();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status']=="true")
		{
		    $json1 = file_get_contents('php://input');
		    $data = json_decode($json1);
		    $language = $data->language;
		  	if($check['data'][0]['admin_status'] == 0)
		  	{ 
			    global $db;
			    $create_date = $data->create_at;
			    $list_type = $data->list_type;
			    $user_id = $check['data'][0]['user_id'];
			    $loadmore = '';
			    if($create_date!=0)
			    {
			        if($list_type == 0)
			        {
			          $loadmore = "AND create_date < '$create_date'";
			        }else
			        {
			          $loadmore = "AND create_date > '$create_date'";
			        }
			    }
				$select_post = $db->customQueryselect("SELECT * FROM notification WHERE receiver_id = '$user_id' ".$loadmore." ORDER BY create_date DESC ");
				if($select_post['status'] == 'success')
				{
					foreach ($select_post['data'] as $key)
					{
						$arr[] = array(
				            'notification_id' =>$key['notification_id'],
				            'sender_id'=>$key['sender_id'],
				            'type'=>$key['type'],
				            'msg'=>$key['msg'],
				            'id'=>$key['id'],
				            'create_at'=>$key['create_date']
				           );
					}
					$select_post['status'] = "success";
					$select_post['message'] = "successfully";
					$select_post['data'] = $arr;
					echoResponse(200,$select_post);
				}else{
					$select_post['status'] = "failed";
					$select_post['message'] = constant('rent_add_'.$language.'_2');
					unset($select_post['data']);
					echoResponse(200,$select_post);
				}        
		  	}
		  	else
		  	{
			    $check['status'] = "1000";
			    $check['message'] = constant('admin_status_'.$language);
			    unset($query_login['data']);
			    echoResponse(200,$check);
		  	}
		}
		else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			unset($check['data']);
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		unset($check['data']);
		echoResponse(200,$check);
	} 
});

$app->post('/garage_work_list_byid',function() use ($app)
{
	$headers = get_header_token();
	if(!empty($headers['secret_key']))
	{
	    $check = token_auth($headers['secret_key']);
	    if($check['status']=="true")
	    {
        	$json1 = file_get_contents('php://input');
	        $data = json_decode($json1);
		    $language = $data->language;
	      	if($check['data'][0]['admin_status'] == 0)
	      	{ 
		        global $db;
		        $user_id = $check['data'][0]['user_id'];
		        $second_user_id = $data->second_user_id;
		        $start_date = $data->create_at;
		        $list_type = $data->list_type;
		 		$start ='';
	            if($start_date != 0 && $start_date != '')
	            {
	              	if($list_type == 0){
	                 	$start = "AND (create_at < '$start_date')";
	              	}else
	              	{
	                  	$start = "AND (create_at > '$start_date')";
	              	}
	            }
	            $reviewsel = $db->customQueryselect("SELECT * FROM garage_work WHERE user_id = '$second_user_id' AND status = 0 AND delete_status = 0 ".$start." ORDER BY create_at DESC LIMIT 10");
	            if($reviewsel['status'] == 'success')
	            {
	                foreach ($reviewsel['data'] as $key) {
	                	$img_arr =array();
		                $seleimage = $db->select("car_image","image",array('mix_id'=>$key['work_id'],'type'=>3));
		                if($seleimage['status']=="success")
		                {
		                     foreach ($seleimage['data'] as $value) {
		                          $img_arr[] = base_url.'uploads/all_post/'.$value['image'];
		                      } 
		                }		
		                $arr[] = array(
                                'add_id' =>$key['work_id'],
                                'user_id' =>$key['user_id'],
                                'car_name'=>$key['car_name'],
                                'model' => $key['model'],
                                'color' => $key['color'],
                                'car_condition' => $key['car_condition'],
                                'car_type'=>$key['car_type'],
                                'version'=>$key['version'],
                                'year' => $key['year'],
                                'mileage'=>$key['mileage'],
                                'problem'=>$key['problem'], 
                                'user_id'=>$key['user_id'],
                                'create_at'=>$key['create_at'],
                                'car_name_id'=>$key['car_name_id'],
                								'model_id'=>$key['model_id'],
                								'version_id'=>$key['version_id'],
                								'delete_status'=>$key['delete_status'],
                								'price'=>$key['price'],
                								'car_modified'=>$key['car_modified'],
                                'image'=>$img_arr
                            );
	                }
                  	$select_post['status'] = "success";
                  	$select_post['message'] = constant('garage_'.$language);
                  	$select_post['data'] = $arr;
                  	echoResponse(200,$select_post);
	            }else
              	{
            	    $select_post['status'] = "failed";
                  	$select_post['message'] =  constant('carname_'.$language);
                  	unset($select_post['data']);
                  	echoResponse(200,$select_post);
              	}
      		}
	    	else
	      	{
		        $check['status'] = "1000";
		        $check['message'] = constant('admin_status_'.$language);
		        unset($query_login['data']);
		        echoResponse(200,$check);
	      	}
    	}
   		else
   		{
		    $check['status'] = "false";
		    $check['message'] = "Invalid Token";
		    unset($check['data']);
		    echoResponse(200,$check);
	   	}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		unset($check['data']);
		echoResponse(200,$check);
	} 
});

$app->post('/notification_detail_by_id', function() use ($app)
{
	$headers = get_header_token();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status']=="true")
		{
				$json1 = file_get_contents('php://input');
				$data = json_decode($json1);
				$language = $data->language;
      if(!empty($json1))
      {
				
				if($check['data'][0]['admin_status'] == 0)
				{
  					global $db;
  					$user_id =  $check['data'][0]['user_id'];
  					$notification_id = $data->notification_id;
  					$select_post = $db->customQueryselect("SELECT notification.type,notification.id,user.user_id,user.avg_rating,user.full_name,user.email,user.mobile_no,user.mobile_code,user.address,user.image as user_image,user.garage_image,user.expertise,user.car_repaire FROM notification INNER JOIN user ON (notification.sender_id = user.user_id AND notification.receiver_id = '$user_id') WHERE notification.notification_id = '$notification_id' AND user.admin_status = 0 AND user.mobile_status = 1");
					if($select_post['status'] == 'success')
					{
					  	foreach ($select_post['data'] as $key)
					  	{
						    $isinterest = 0; $img_arr =array(); $problem='';
						    $interest = $db->select("interest","interest_id,problem",array('user_id'=>$key['user_id'],'second_user_id'=>$user_id,'type'=>$key['type'],'interested_id'=>$key['id']));
						    if($interest['status']=="success")
						    {
						        $isinterest = 1;
						        $problem = $interest['data'][0]['problem'];
						    }
					     
						    if($key['user_image']!='')
						    {
						      $key['user_image'] = base_url.'uploads/user_image/'.$key['user_image'];
						    }
						    if($key['type']==3)
						    {
						        if($key['garage_image']!='')
						        {
						            $key['garage_image'] = base_url.'uploads/garage_image/'.$key['garage_image'];
						        }
						    }else
						    {
						        $seleimage = $db->select("car_image","image",array('mix_id'=>$key['id'],'type'=>$key['type']));
						        if($seleimage['status']=="success")
						        {
						            foreach ($seleimage['data'] as $value) {
						              $img_arr[] = base_url.'uploads/all_post/'.$value['image'];
						            } 
						        }
						    }
						    if(isset($key['text']))
						    {
						        $key['description'] = $key['text'];
						        unset($key['text']);
						    }
						    $key['is_interest'] = $isinterest;
						    $key['problem'] = $problem;
						    $key['image'] = $img_arr; 
						    if($key['type']==1)
						    {
						        $selecttable = $db->select("add_post","*",array('add_id'=>$key['id']));
						    }elseif($key['type']==2)
						    {
						        $selecttable = $db->select("car_rent","*",array('rent_id'=>$key['id']));
						    }elseif($key['type']==3)
						    {
						        $selecttable =array('status'=>'failed');
						        //$selecttable = $db->select("garage_work","*",array('add_id'=>$key['id']));
						    }
						    unset($key['id']);
						    $obj1 = $key; 
						    if($selecttable['status']=="success")
						    {
						        $obj2 = $selecttable['data'][0];
						        if($key['type']==2){ $obj2['add_id'] = $obj2['rent_id']; $obj2['description'] = $obj2['text']; unset($obj2['text']); unset($obj2['rent_id']); }
						    }else
						    {
						        $obj2 = array(
						            "add_id"=> "",
						            "car_name"=> "",
						            "car_name_id"=> "",
						            "model"=> "",
						            "model_id"=> "",
						            "color"=> "",
						            "car_condition"=> "",
						            "car_type"=> "",
						            "version"=> "",
						            "version_id"=> "",
						            "year"=> "",
						            "mileage"=> "",
						            "price"=> "",
						            "status"=> "",
						            "delete_status"=> "",
						            "create_at"=> "",
						            "update_at"=> ""
						          );
						    }
					   	 	$mergeobj = (object) array_merge((array) $obj1, (array) $obj2);
					  	}
					    $select_post['status'] = "success";
					    $select_post['message'] = "successfully";
					    $select_post['data'] = $mergeobj;
					    echoResponse(200,$select_post);
					}else{
						$select_post['status'] = "failed";
						$select_post['message'] = constant('rent_add_'.$language.'_2');
						unset($select_post['data']);
						echoResponse(200,$select_post);
					}
				}else
				{
					$select_post['status'] = "1000";
					$select_post['message'] = constant('admin_status_'.$language);
					unset($select_post['data']);
					echoResponse(200,$select_post);
				}
			}
			else
			{ 
				$check['status'] = "failed";
				$check['message'] ="No Request parameter";
				unset($check['data']);
				echoResponse(200,$check);
			}         
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			unset($check['data']);
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		unset($check['data']);
		echoResponse(200,$check);
	} 
});

$app->post('/test',function() use ($app){

  $message = 'Reset Password OTP';
  $subject = "Spectre App: Forgot Password";
  $email_from ='Barbers@Kwikuts.co.uk';
  // $headers  = 'MIME-Version: 1.0' . "\r\n";
  // $headers .= 'Content-type: text/html; charset=iso-8859-1'. "\r\n";
  // $headers .= 'From: '.$email_from. '\r\n';
  $cc = '';
  //@mail($mobile, $subject, $message,$headers);   
  $aa =  Send_Mail($email_from,'jaipal.solanki@ebabu.co',$subject,$message);  

});       
// $app->post('/garage_work_list_detail', function() use ($app){
//   $headers = get_header_token();
//   if(!empty($headers['secret_key']))
//   {
//   	$check = token_auth($headers['secret_key']);

//   	if($check['status']=="true" || $headers['secret_key']=='ef73781effc5774100f87fe2f437a435')
//     {
//       	//if($check['data'][0]['admin_status'] == 0)
//       	//{  
//          $json1 = file_get_contents('php://input');
//          if(!empty($json1))
//          {
//            	$data = json_decode($json1);
            
//            	$start_date = $data->create_at;
//            	$list_type = $data->list_type;

//             global $db;
//             $user_id ='';
//             if($check['status']=='true')
//             {
//             	$user_id =  $check['data'][0]['user_id'];
//             } 
              
//             $start ='';
//             if($start_date != 0 && $start_date != '')
//             {
//               	if($list_type == 0){
//                  	$start = "AND (create_at < '$start_date')";
//               	}else
//               	{
//                   	$start = "AND (create_at > '$start_date')";
//               	}
//             }
            
//             $select_post = $db->customQueryselect("SELECT garage_work.*,user.full_name,user.email,user.mobile_no,user.address,user.image,user.expertise,user.car_repaire,user.create_at as create_date FROM user LEFT JOIN garage_work ON user.user_id = garage_work.user_id WHERE user.type = 2 AND user.admin_status = 0 AND user.mobile_status = 1 AND user.create_at != '' ".$start." GROUP BY garage_work.user_id ORDER BY user.create_at DESC LIMIT 10");
//             if($select_post['status'] == 'success')
//             {
//                foreach ($select_post['data'] as $key)
//                {
//                   $img_arr =array();
//                   if(isset($key['work_id'])){
// 	                  $seleimage = $db->select("car_image","image",array('mix_id'=>$key['work_id'],'type'=>3));
// 	                  if($seleimage['status']=="success")
// 	                  {
// 	                     foreach ($seleimage['data'] as $value) {
// 	                          $img_arr[] = base_url.'uploads/all_post/'.$value['image'];
// 	                      } 
// 	                  }
//                   }
//                   $uimage = '';
//                   if($key['image']!='')
//                   {
//                   	$uimage = base_url.'uploads/user_image/'.$key['image'];
//                   }		
//                   $revie = $workdata =array();
//                   //echo $key['user_id'];exit;

//                   $reviewsel = $db->customQueryselect("SELECT rating,reviews FROM rating_n_review WHERE second_user_id = '".$key['user_id']."' ORDER BY id DESC LIMIT 2");
//                   if($reviewsel['status']=="success")
//                   {
//                       foreach ($reviewsel['data'] as $value) {
//                          $revie[] = $value;
//                       }
//                   }
//                   $reviewsel = $db->customQueryselect("SELECT add_id,user_id,car_name,car_name_id,model,model_id,version,color,car_condition,car_type,year,mileage,problem,delete_status,price,car_modified FROM garage_work WHERE user_id = '".$key['user_id']."' AND status = 0 AND delete_status = 0 ORDER BY work_id DESC LIMIT 2");
//                   if($reviewsel['status']=="success")
//                   {
//                       foreach ($reviewsel['data'] as $value) {
//                          $workdata[] = $value;
//                       }
//                   }
//                   $arr[] = array(
// 	                        'add_id' =>$key['work_id'],
// 	                        'user_id' =>$key['user_id'],
// 	                        'car_name'=>$key['car_name'],
// 	                        'car_name_id'=>$key['car_name_id'],
// 	                        'model' => $key['model'],
// 					        'model_id'=>$key['model_id'],
//                           	'version'=>$key['version'],
// 					        'version_id'=>$key['version_id'],
//                           	'color' => $key['color'],
//                           	'car_condition' => $key['car_condition'],
//                           	'car_type'=>$key['car_type'],
//                           	'year' => $key['year'],
//                           	'mileage'=>$key['mileage'],
//                           	'problem'=>$key['problem'], 
// 							'delete_status'=>$key['delete_status'],
// 							'price'=>$key['price'],
// 							'car_modified'=>$key['car_modified'],
// 							'full_name'=>$key['full_name'],
// 							'email'=>$key['email'],
// 							'mobile_no'=>$key['mobile_no'],
// 							'address'=>$key['address'],
// 							'expertise'=>$key['expertise'],
// 							'car_repaire'=>$key['car_repaire'],
// 	                        'user_image'=>$uimage,
// 	                        'create_at'=>$key['create_date'],
// 	                        'reviews'=>$revie,
// 	                        'image'=>$img_arr,
// 	                        'work'=>$workdata
//                             );
//                 }
//                   $select_post['status'] = "success";
//                   $select_post['message'] = "successfully";
//                   $select_post['data'] = $arr;
//                   echoResponse(200,$select_post);
//             }else{
//               $select_post['status'] = "failed";
//               $select_post['message'] ="No list found";
//               unset($select_post['data']);
//               echoResponse(200,$select_post);
//             }
//         }
//         else
//         { 
//           $check['status'] = "failed";
//           $check['message'] ="No Request parameter";
//           unset($check['data']);
//           echoResponse(200,$check);
//         }         
//       // }else
//       // {
//       //   $check['status'] = "1000";
//       //   $check['message'] = "Your spectre account has been temporarily suspended as a security precaution.";
//       //   unset($check['data']);
//       //   echoResponse(200,$check);
//       // }
//     }else
//     {
//       $check['status'] = "false";
//       $check['message'] = "Invalid Token";
//       unset($check['data']);
//       echoResponse(200,$check);
//     }
//   }else
//   {
//     $check['status'] = "false";
//      $check['message'] = "Unauthorised access";
//     unset($check['data']);
//     echoResponse(200,$check);
//   } 
// });


function get_value($value)
{
   return  $value + 1;
}
function echoResponse($status_code, $response){
    global $app;
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response);
}
$app->run();
?>

